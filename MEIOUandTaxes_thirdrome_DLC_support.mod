name="M&T Third Rome DLC Support"
path="mod/MEIOUandTaxes_thirdrome_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesTR.jpg"
supported_version="1.24.*.*"
