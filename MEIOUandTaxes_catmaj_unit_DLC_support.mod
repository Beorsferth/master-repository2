name="M&T Thirty Years' War (Evangelical Union and Catholic League) Majors DLC Support"
path="mod/MEIOUandTaxes_catmaj_unit_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesTYW.jpg"
supported_version="1.24.*.*"
