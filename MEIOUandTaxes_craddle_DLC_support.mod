name="M&T Cradle of Civilisation DLC Support"
path="mod/MEIOUandTaxes_craddle_DLC_support"
dependencies={
	"MEIOU and Taxes 2.02"
}
tags={
	"MEIOU and Taxes"
}
picture="MEIOUandTaxesCoC.jpg"
supported_version="1.24.*.*"
