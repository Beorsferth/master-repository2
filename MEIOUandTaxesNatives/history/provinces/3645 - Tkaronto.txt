# No previous file for Tkaronto

owner = HUR
controller = HUR
add_core = HUR
is_city = yes
culture = huron
religion = totemism
capital = "Tkaronto"
trade_goods = unknown
hre = no 
base_tax = 1 base_production = 1 base_manpower = 1
native_size = 35
native_ferocity = 2
native_hostileness = 5

1609.1.1  = {  } # Samuel de Champlain.
1629.1.1  = {  }
1649.1.1  = {
 	owner = IRO
	controller = IRO
	add_core = IRO
	culture = iroquois
} #Taken by Iroquois in Beaver Wars.
1674.1.1  = {	owner = FRA
		controller = FRA
		culture = francien
		religion = catholic
		citysize = 1900
	    } # Construction of Fort Frontenac
1674.1.1  = { fort_14th = yes } # Fort Frontenac
1697.1.1  = { add_core = FRA }
1700.1.1  = { citysize = 4780 }
1707.5.12 = {  } 
1750.1.1 = {	
	capital = "Fort Rouill�"
	is_city = yes
	add_core = FRA
}
1763.2.10 = {	owner = GBR
		controller = GBR
		remove_core = FRA
	    } # Treaty of Paris - ceded to Britain, France gave up its claim
1783.1.1  = { 	culture = english
		religion = protestant 
	} #Loyalist migration after the revolution
1788.2.10 = { add_core = GBR }
