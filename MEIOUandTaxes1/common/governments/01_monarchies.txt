##############################   Monarchy    ##############################
##############################  Governments  ##############################
medieval_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	allow_vassal_war = yes
	allow_vassal_alliance = yes
	
	color = { 179 25 25 }
	
	ai_will_do = {
		factor = 0
	}
	ai_importance = 0
	
	max_states = 1
	
	#bonus
	rank = {
		1 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.05
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
		2 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.05
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
		3 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.05
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
		4 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.05
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
		5 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.1
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
		6 = {
			vassal_income = 0.25
			land_maintenance_modifier = -0.05
			land_forcelimit = -2 # Added to the estates forcelimit instead
			
			land_forcelimit_modifier = 0.1
			fort_maintenance_modifier = -0.20
			
			global_autonomy = 0.10
		}
	}
}

despotic_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	#allow_vassal_war = yes
	#allow_vassal_alliance = yes
	
	color = { 179 50 50 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = administrative_monarchy }
			NOT = { government = absolute_monarchy }
			NOT = { government = constitutional_monarchy }
			NOT = { government = enlightened_despotism }
		}
		modifier = {
			factor = 0
			government = republic
		}
	}
	ai_importance = 1
	
	max_states = 0
	
	#bonus
	rank = {
		1 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
		2 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
		3 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
		4 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
		5 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
		6 = {
			stability_cost_modifier = 0.15
			unjustified_demands = -0.1
			land_forcelimit_modifier = 0.1
			global_manpower_modifier = 0.1
			
			land_maintenance_modifier = -0.10
			
			global_autonomy = 0.0
		}
	}
}

feudal_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 75 75 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = despotic_monarchy }
			NOT = { government = absolute_monarchy }
			NOT = { government = constitutional_monarchy }
			NOT = { government = enlightened_despotism }
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = despotic_monarchy
		}
		modifier = {
			factor = 5
			has_idea = grand_army
			has_idea = glorious_arms
			has_idea = battlefield_commisions
			has_idea = improved_foraging
			#has_idea = improved_manuever
			#has_idea = napoleonic_warfare
		}
	}
	ai_importance = 2
	
	max_states = 2
	
	#bonus
	rank = {
		1 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
		2 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
		3 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
		4 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
		5 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
		6 = {
			vassal_income = 0.125
			land_maintenance_modifier = -0.05
			
			fort_maintenance_modifier = -0.15
			
			global_autonomy = 0.05
		}
	}
}

administrative_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 100 100 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = despotic_monarchy }
			NOT = { government = medieval_monarchy }
			NOT = { government = feudal_monarchy }
			NOT = { government = constitutional_monarchy }
			NOT = { government = enlightened_despotism }
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = medieval_monarchy
		}
		modifier = {
			factor = 5
			government = feudal_monarchy
		}
		modifier = {
			factor = 5
			has_idea = organised_mercenary_payment
			has_idea = benefits_for_mercenaries
			has_idea = organised_mercenary_recruitment
			has_idea = resilient_state
			has_idea = war_cabinet
		}
	}
	ai_importance = 3
	
	max_states = 3
	
	#bonus
	rank = {
		1 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
		2 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
		3 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
		4 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
		5 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
		6 = {
			production_efficiency = 0.05
			global_tax_modifier = 0.05
			
			global_autonomy = 0.0
			max_absolutism = 5
		}
	}
}

absolute_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 125 125 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = despotic_monarchy }
			NOT = { government = medieval_monarchy }
			NOT = { government = feudal_monarchy }
			NOT = { government = administrative_monarchy }
			NOT = { government = enlightened_despotism }
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = administrative_monarchy
		}
		modifier = {
			factor = 5
			has_idea = engineer_corps
			has_idea = military_drill
			has_idea = regimental_system
			has_idea = defensive_mentality
			#has_idea = superior_firepower
			has_idea = supply_trains
			has_idea = national_conscripts
		}
	}
	ai_importance = 4
	
	max_states = 2
	
	#bonus
	rank = {
		1 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
		2 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
		3 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
		4 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
		5 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
		6 = {
			stability_cost_modifier = 0.30
			discipline = 0.05
			
			global_autonomy = -0.05
		}
	}
}

constitutional_monarchy = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 150 150 }
	
	has_parliament = yes
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			NOT = { government = despotic_monarchy }
			NOT = { government = medieval_monarchy }
			NOT = { government = feudal_monarchy }
			NOT = { government = administrative_monarchy }
			NOT = { government = absolute_monarchy }
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = absolute_monarchy
		}
		modifier = {
			factor = 5
			has_idea = foreign_embassies
			has_idea = claim_fabrication
			#has_idea = cabinet
			has_idea = adaptability
			has_idea = benign_diplomats
			has_idea = diplomatic_influence
			has_idea = flexible_negotiation
		}
	}
	ai_importance = 4
	
	max_states = 3
	
	#bonus
	rank = {
		1 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
		2 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
		3 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
		4 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
		5 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
		6 = {
			stability_cost_modifier = -0.10
			legitimacy = 0.15
			
			global_autonomy = -0.05
			max_absolutism = -5
		}
	}
}

enlightened_despotism = {
	monarchy = yes
	
	valid_for_new_country = yes
	valid_for_nation_designer = yes
	nation_designer_cost = 20
	
	color = { 179 175 175 }
	
	ai_will_do = {
		factor = 1
		modifier = {
			factor = 0
			government = revolutionary_empire
		}
		modifier = {
			factor = 0
			government = republic
		}
		modifier = {
			factor = 5
			government = constitutional_monarchy
		}
		modifier = {
			factor = 5
			has_idea = grand_army
			has_idea = glorious_arms
			has_idea = battlefield_commisions
			has_idea = improved_foraging
			#has_idea = improved_manuever
			#has_idea = napoleonic_warfare
		}
	}
	ai_importance = 5
	
	max_states = 1
	
	#bonus
	rank = {
		1 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
		2 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
		3 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
		4 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
		5 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
		6 = {
			num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
			core_creation = -0.05
			global_manpower_modifier = 0.1
			
			
			global_autonomy = 0.0
		}
	}
}

#Special for England
english_monarchy = {
	monarchy = yes
	
	color = { 200 25 25 }
	
	unique_government = yes
	
	valid_for_new_country = no
	allow_convert = no
	
	valid_for_nation_designer = yes
	nation_designer_cost = 15
	nation_designer_trigger = {
		has_dlc = "Common Sense"
	}
	
	has_parliament = yes
	
	max_states = 2
	
	rank = {
		1 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
		2 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
		3 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
		4 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
		5 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
		6 = {
			legitimacy = 0.15
			global_unrest = -1
			
			global_autonomy = 0.05
		}
	}
}
