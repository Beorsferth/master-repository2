# These ideas are loaded after all country ideas, but before default idea.

client_ideas = {
	start = {
		stability_cost_modifier = -0.05
		core_creation = -0.10
	}
	
	bonus = {
		build_cost = -0.10
	}
	
	trigger = {
		is_client_nation = yes
		always = no
	}
	
	free = yes
	
	client_self_rule = {
		global_unrest = -1
	}
	foreign_tax_collectors = {
		global_tax_modifier = 0.10
	}
	fortified_borders = {
		defensiveness = 0.15
	}
	magnate_landowners = {
		production_efficiency = 0.1
	}
	mercantile_privileges = {
		global_prov_trade_power_modifier = 0.15
	}
	mercenary_recruitment_center = {
		mercenary_cost = -0.15
	}
	foreign_advisors_client = {
		advisor_pool = 1
	}
}

colonial_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		is_colonial_nation = yes
	}
	
	free = yes
	
	generic_trait = {
	}
	trade_company_trait = {
		defensiveness = 0.05
	}
	new_frontiers = {
		global_prov_trade_power_modifier = 0.15
		province_warscore_cost = -0.1
		may_establish_frontier = yes
	}
	colonial_militia = {
		infantry_cost = -0.05
		infantry_power = 0.05
	}
	chartered_company = {
		republican_tradition = 0.20
		prestige = 0.15
	}
	religious_outposts = {
		global_missionary_strength = 0.02
		tolerance_heretic = 1.5
	}
	founding_fathers = {
		advisor_cost = -0.05
		idea_cost = -0.05
	}
}

native_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		government = native_council
	}
	
	free = yes
	
	anomist_trait = {
	}
	native_american_idea = {
		prestige_from_land = 0.25
		auto_explore_adjacent_to_colony = yes
	}
	counting_coups = {
		land_morale = 0.05
		manpower_recovery_speed = 0.1
	}
	storytelling_tradition = {
		idea_cost = -0.05
		prestige = 0.10
		stability_cost_modifier = -0.10
	}
	the_little_war = {
		global_unrest = -2
		global_autonomy = -0.025
	}
	war_and_peace_chiefs = {
		global_manpower_modifier = 0.10
		diplomatic_upkeep = 1
	}
	great_winter_hunts = {
		global_colonial_growth = 5
		production_efficiency = 0.10
	}
}

rajput_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = rajput
		NOT = { primary_culture = gujarati }
		NOT = { primary_culture = saurashtri }
		NOT = { religion_group = muslim }
	}
	
	free = yes
	
	communism_trait = {
	}
	north_indian_idea = {
		global_unrest = -0.5
	}
	clan_loyalties = {
		global_manpower_modifier = 0.05
		manpower_recovery_speed = 0.05
	}
	fortifying_rajputana = {
		defensiveness = 0.10
		fort_maintenance_modifier = -0.10
		enemy_core_creation = 0.25
	}
	marwari_horses = {
		cavalry_power = 0.05
		discipline = 0.015
	}
	rajput_painting_school = {
		prestige = 0.20
		idea_cost = -0.05
	}
	marwari_trading_houses = {
		global_foreign_trade_power = 0.1
		trade_efficiency = 0.10
		diplomatic_upkeep = 1
	}
}

dravidian_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			culture_group = dravidian
			primary_culture = telegu
			primary_culture = kannada
		}
		religion = hinduism
	}
	
	free = yes
	
	indian_trait = {
	}
	south_indian_idea = {
		dip_tech_cost_modifier = -0.025
	}
	merchants_of_southern_india = {
		trade_range_modifier = 0.25
		trade_efficiency = 0.05
		global_foreign_trade_power = 0.1
		global_ship_trade_power = 0.10
	}
	water_tanks_and_canals = {
		global_tax_modifier = 0.05
		production_efficiency = 0.10
		build_cost = -0.10
		global_prov_trade_power_modifier = 0.05
	}
	polygar_system = {
		global_manpower_modifier = 0.1
		global_sailors_modifier = 0.1
	}
	south_indian_mercenaries = {
		mercenary_cost = -0.05
		possible_mercenaries = 0.25
	}
	patronage_of_temples = {
		legitimacy = 0.10
		prestige = 0.10
		stability_cost_modifier = -0.05
		tolerance_heathen = 1
	}
}

bengali_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = bengali
	}
	
	free = yes	# Will be added at load.
	
	communism_trait = {
	}
	north_indian_idea = {
		global_unrest = -0.5
	}
	hindu_sufi_syncretism = {
		tolerance_heathen = 1
		religious_unity = 0.10
		mil_tech_cost_modifier = -0.05
	}
	ganges_brahmaputra_confluence = {
		trade_efficiency = 0.1
		merchants = 1
	}
	rice_fields = {
		global_manpower_modifier = 0.1
		production_efficiency = 0.1
	}
	mustard_oil_ilish_mach = {
		war_exhaustion_cost = -0.10
		infantry_power = 0.05
		land_morale = 0.025
	}
	bengali_renaissance = {
		idea_cost = -0.05
		adm_tech_cost_modifier = -0.05
		prestige = 0.15
	}
}

gujarati_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			AND = {
				religion = hinduism
				primary_culture = saurashtri
			}
			primary_culture = gujarati
		}
	}
	
	free = yes	# Will be added at load.
	
	communism_trait = {
	}
	north_indian_idea = {
		global_unrest = -0.5
	}
	garba = {
		tolerance_heretic = 1
		tolerance_heathen = 1
		religious_unity = 0.05
	}
	jain_routes = {
		trade_range_modifier = 0.25
		trade_efficiency = 0.05
		global_foreign_trade_power = 0.1
		global_ship_trade_power = 0.10
	}
	south_indian_mercenaries = {
		mercenary_cost = -0.05
		possible_mercenaries = 0.25
	}
	merchant_diaspora = {
		light_ship_cost = -0.1
		merchants = 1
	}
	spice_trade = {
		global_prov_trade_power_modifier = 0.1
		diplomatic_upkeep = 1
	}
}

indian_muslim_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		religion_group = muslim
		OR = {
			culture_group = eastern_aryan
			culture_group = hindusthani
			culture_group = central_indian
			culture_group = pahari_group
			culture_group = deccan_group
			culture_group = rajput
			culture_group = dravidian
		}
	}
	
	free = yes
	
	communism_trait = {
	}
	north_indian_idea = {
		global_unrest = -0.5
	}
	tolerate_idol_worshippers_i = {
		tolerance_heathen = 1.5
		religious_unity = 0.10
	}
	islamic_soldiers = {
		cavalry_power = 0.05
		mil_tech_cost_modifier = -0.05
	}
	persian_court = {
		idea_cost = -0.05
		global_unrest = -1
		advisor_pool = 1
	}
	equality_under_law = {
		global_trade_goods_size_modifier = 0.025
		global_unrest = -1
		stability_cost_modifier = -0.05
	}
	legacy_of_conquest = { #invaders from Afghanistan, borders expanded and contracted greatly over the ages
		prestige_from_land = 0.25
		legitimacy = 0.10
		prestige = 0.10
		diplomatic_reputation = 0.5
	}
}

horde_ideas = {
	start = {
	}
	bonus = {
	}
	trigger = {
		OR = {
			government = tribal_nomads_altaic
			government = tribal_nomads_steppe
		}
		NOT = { tag = YUA }
	}
	
	free = yes
	
	communism_trait = { #25
	}
	cavalry_riders_trait = {
		cav_to_inf_ratio = 0.10
		cavalry_flanking = 0.15
		mil_tech_cost_modifier = 0.05
	}
	life_of_steppe_warrior = {
		land_attrition = -0.05
		prestige_from_land = 0.25
		army_tradition_from_battle = 0.25
	}
	traditions_of_the_great_khan = {
		discipline = 0.02
		cavalry_power = 0.10
		infantry_power = -0.05
	}
	tradition_of_conquest = {
		core_creation = -0.1
		loot_amount = 0.25
	}
	logistics_of_khan = {
		global_autonomy = -0.01
		diplomatic_reputation = 0.5
		global_unrest = -1
	}
	horse_supplies = {
		reinforce_cost_modifier = -0.05
	}
}

theocracy_ideas = {
	start = {
		manpower_recovery_speed = 0.1
		missionaries = 1
	}
	
	bonus = {
		global_manpower_modifier = 0.15
	}
	
	trigger = {
		always = no
		government = theocracy
		religion_group = christian
	}
	
	free = yes
	
	holy_order = {
		discipline = 0.025
	}
	church_administration = {
		stability_cost_modifier = -0.050
		core_creation = -0.10
	}
	true_defender_of_the_faith = {
		defensiveness = 0.20
	}
	no_witches = {
		global_missionary_strength = 0.02
	}
	onward_christian_soldiers = {
		global_garrison_growth = 0.1
	}
	turn_the_other_cheek = {
		hostile_attrition = 1
	}
	render_unto_ceasar = {
		global_tax_modifier = 0.10
	}
}

anatolian_beyliks_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = turkish
			primary_culture = yorouk
			tag = ERE
		}
		NOT = { tag = OTT }
		NOT = { tag = TUR }
	}
	
	free = yes
	
	endogamy_trait = {
	}
	beylik_architecture = {
		build_cost = -0.05
		prestige = 0.05
	}
	turkish_language = {
		religious_unity = 0.05
		num_accepted_cultures = 1
	}
	ghazi = {
		manpower_recovery_speed = 0.05
	}
	mahalle = {
		global_unrest = -1
		production_efficiency = 0.10
	}
	istimalet = {
		tolerance_heretic = 1
		tolerance_heathen = 1.5
	}
	caravanserais = {
		global_prov_trade_power_modifier = 0.05
		global_foreign_trade_power = 0.1
	}
}

daimyo_ideas = {
	start = {
		
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			government = japanese_monarchy
			culture_group = japanese
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	japan_trait = {
		infantry_power = 0.03
	}
	gunki_monogatari = {
		prestige_from_land = 0.25
		prestige_decay = -0.005
		institution_spread_from_true_faith = 0.05
	}
	five_mountain_system = {
		tolerance_own = 1
		build_cost = -0.10
	}
	kemmu_restoration = {
		legitimacy = 0.10
		land_forcelimit_modifier = 0.05
		diplomatic_upkeep = 1
	}
	rise_of_shugo = {
		defensiveness = 0.05
		spy_offence = 0.05
		global_tax_modifier = 0.05
		diplomatic_reputation = 0.5
	}
	the_katana_idea = {
		army_tradition_decay = -0.005
		army_tradition_from_battle = 0.25
		shock_damage = 0.1
	}
}

swabian_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = schwabisch
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	semi_authoritarian_trait = {
	}
	ger_alemanish_culture = {
		diplomatic_reputation = 0.25
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	swabian_league_idea = {
		republican_tradition = 0.10
		improve_relation_modifier = 0.15
		diplomatic_reputation = 0.5
	}
	meistersinger_idea = {
		adm_tech_cost_modifier = -0.05
		prestige = 0.15
		advisor_cost = -0.025
	}
}

rhein_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = rhine_alemanisch
			primary_culture = hessian
		}
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	semi_authoritarian_trait = {
	}
	ger_alemanish_culture = {
		diplomatic_reputation = 0.25
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

franconian_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = eastfranconian
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_low_frankish_culture = {
		trade_range_modifier = 0.20
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.1
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	meistersinger_idea = {
		adm_tech_cost_modifier = -0.05
		prestige = 0.15
		advisor_cost = -0.025
	}
}

burgundian_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = wallonian
			primary_culture = brabantian
		}
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	semi_authoritarian_trait = {
	}
	ger_alemanish_culture = {
		diplomatic_reputation = 0.25
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

westphalian_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = westphalian
			primary_culture = ripuarianfranconian
		}
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_low_frankish_culture = {
		trade_range_modifier = 0.20
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.1
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

german_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = old_saxon
			primary_culture = eastphalian
			primary_culture = low_saxon
			primary_culture = baltendeutsche
			primary_culture = pommeranian
			primary_culture = thuringian
			primary_culture = high_saxon
		}
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_low_frankish_culture = {
		trade_range_modifier = 0.20
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.1
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

bavarian_circle_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = bavarian
		OR = {
			government = imperial_city
			government = theocratic_government
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_austrian_culture = {
		adm_tech_cost_modifier = -0.025
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

german_alemanish_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = moselfranconian
			primary_culture = rhine_alemanisch
			primary_culture = schwabisch
			primary_culture = ripuarianfranconian
			primary_culture = wallonian
			primary_culture = brabantian
			primary_culture = hessian
		}
	}
	
	free = yes	# Will be added at load.
	
	semi_authoritarian_trait = {
	}
	ger_alemanish_culture = {
		diplomatic_reputation = 0.25
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

german_south_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = bavarian
			primary_culture = eastfranconian
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_austrian_culture = {
		adm_tech_cost_modifier = -0.025
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.20
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

german_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			tag = SHL
			culture_group = low_germanic
			culture_group = high_germanic
		}
	}
	
	free = yes	# Will be added at load.
	
	authoritarian_trait = {
	}
	ger_low_frankish_culture = {
		trade_range_modifier = 0.20
	}
	german_land_of_mercenaries = {
		infantry_power = 0.025
		mercenary_discipline = 0.025
		possible_mercenaries = 0.50
	}
	zunft = {
		production_efficiency = 0.20
		idea_cost = -0.025
	}
	german_trade_routes = {
		global_own_trade_power = 0.1
		global_foreign_trade_power = 0.1
	}
	german_feudalism = {
		global_autonomy = -0.025
		improve_relation_modifier = 0.15
		vassal_income = 0.05
	}
	carolingian_heritage = {
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
		prestige = 0.10
	}
}

irish_gaelic_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = irish
	}
	
	free = yes
	
	endogamy_trait = { #25p
	}
	british_islands = {
		transport_cost = -0.10
	}
	the_clanns = {
		enemy_core_creation = 0.25
		stability_cost_modifier = -0.10
	}
	brehon_law_idea = {
		heir_chance = 0.5
		global_unrest = -2
		adm_tech_cost_modifier = -0.05
	}
	gall_oglaigh = {
		mercenary_discipline = 0.025
		possible_mercenaries = 0.25
	}
	european_influences_irish = {
		prestige = 0.05
		idea_cost = -0.05
		improve_relation_modifier = 0.25
	}
	beyond_the_pale = {
		land_morale = 0.10
	}
}

irish_normand_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = hiberno_norman
	}
	
	free = yes
	
	endogamy_trait = { #25p
	}
	british_islands = {
		transport_cost = -0.10
	}
	the_clanns = {
		enemy_core_creation = 0.25
		stability_cost_modifier = -0.10
	}
	irish_parliament = {
		global_unrest =-2
		adm_tech_cost_modifier = -0.05
		legitimacy = 0.10
	}
	tower_houses_idea = {
		defensiveness = 0.10
		prestige_from_land = 0.50
	}
	dominus_hiberniae = {
		fabricate_claims_cost = -0.10
		diplomatic_reputation = 1
	}
	beyond_the_pale = {
		land_morale = 0.10
	}
}

malayan_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			primary_culture = dayak
			culture_group = malay
			culture_group = javan_group
			culture_group = filipino_group
		}
		NOT = { capital_scope = { region = sumatra_region } }
	}
	
	free = yes
	
	anomist_trait = {
	}
	javan_idea = {
		transport_cost = -0.10
	}
	indian_ocean_trade = {
		trade_efficiency = 0.10 #6
		trade_range_modifier = 0.25 #5
		global_foreign_trade_power = 0.1 #7
	}
	mal_spice_islands = {
		production_efficiency = 0.1 #7
		global_trade_goods_size_modifier = 0.05 #16
	}
	classical_malay = {
		idea_cost = -0.05 #10
		diplomatic_upkeep = 1 #7
	}
	trading_fleets = {
		galley_power = 0.05 #7
		naval_morale = 0.05
		global_ship_trade_power = 0.10 #8
		global_sailors_modifier = 0.1 #9
	}
	heirs_of_pasai = {
		prestige = 0.10 #4
		stability_cost_modifier = -0.05 #6
		land_morale = 0.05
	}
}

arabian_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			culture_group = arabian
			culture_group = mashreqi
			AND = {
				primary_culture = nubian
				religion_group = muslim
			}
		}
	}
	
	free = yes
	
	endogamy_trait = { #25
	}
	cavalry_riders_trait = {
		cav_to_inf_ratio = 0.10
		cavalry_flanking = 0.15
		mil_tech_cost_modifier = 0.05
	}
	bedouin_traders = {
		caravan_power = 0.05
		dip_tech_cost_modifier = -0.025
	}
	arabian_horses = {
		cavalry_power = 0.05
		cavalry_cost = -0.025
		cav_to_inf_ratio = 0.10
		army_tradition_from_battle = 0.25
		infantry_power = -0.025
	}
	land_of_the_prophet = {
		legitimacy = 0.10
		tolerance_own = 1
	}
	spreading_the_prophets_word = {
		global_missionary_strength = 0.02
		spy_offence = 0.05
	}
	arab_indian_ocean_trade = {
		global_foreign_trade_power = 0.1
		trade_range_modifier = 0.25
		trade_efficiency = 0.05
	}
}

latin_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = latin
		capital_scope = {
			OR = {
				region = east_italy_region
				region = west_italy_region
				region = central_italy_region
			}
		}
	}
	
	free = yes
	
	generic_trait = { #25
	}
	ita_neutral_trait = {
		prestige = 0.025
		papal_influence = 0.5
	}
	trecento_renaissance = {
		adm_tech_cost_modifier = -0.1
		idea_cost = -0.05
	}
	condotierri_idea = {
		mercenary_discipline = 0.025
		merc_maintenance_modifier = -0.05
	}
	pilgrimage_route_idea = {
		trade_range_modifier = 0.3
		diplomatic_upkeep = 1
		global_trade_power = 0.05
	}
	italian_aristocracy = {
		enemy_core_creation = 0.15
		global_manpower_modifier = 0.10
	}
	tracee_italienne = {
		defensiveness = 0.1
		garrison_size = 0.1
		mil_tech_cost_modifier = -0.05
	}
}

ruthenian_ideas = {
	start = {
	}
	bonus = {
	}
	
	trigger = {
		primary_culture = ruthenian
	}
	
	free = yes
	
	communism_trait = { #25
	}
	cavalry_riders_trait = {
		cav_to_inf_ratio = 0.10
		cavalry_flanking = 0.15
		mil_tech_cost_modifier = 0.05
	}
	russkaya_pravda = { #21
		stability_cost_modifier = -0.05
		global_unrest = -2
		core_creation = -0.10
	}
	mother_of_all_cities = {
		prestige = 0.1 #6
		tolerance_own = 0.5 #8
		diplomatic_reputation = 0.5 #8
	}
	conscription_of_boyar_sons = {
		cavalry_cost = -0.03 #8
		cavalry_power = 0.05 #14
	}
	cossack_ethos = {
		land_morale = 0.1 #9
		defensiveness = 0.10 #9
		land_attrition = -0.025 #7
	}
	rabstvo_ground = {
		production_efficiency = 0.1 #7
		global_manpower_modifier = 0.05 #8
	}
}

french_minor_ideas = {
	start = {
	}
	bonus = {
	}
	
	trigger = {
		OR = {
			culture_group = langue_d_oil
			tag = BOU
			tag = ACH
		}
		NOT = { tag = KEG }
		#NOT = { tag = KRB }
		NOT = { tag = LOT }
		NOT = { tag = MTV }
		NOT = { primary_culture = french_colonial }
	}
	
	free = yes
	
	equality_trait = { #25
	}
	loire_river_trait = {
		idea_cost = -0.02
	}
	lingua_franca = {
		diplomatic_upkeep = 1 #7
		vassal_income = 0.10 #20
	}
	french_geography = {
		diplomatic_reputation = 1 #16
	}
	french_chivalry = { #33
		cavalry_power = 0.05
		shock_damage = 0.10
		army_tradition_from_battle = 0.25
		prestige_from_land = 0.25
	}
	estates_general = { #17
		global_tax_modifier = 0.1
		production_efficiency = 0.075
	}
	castle_warfare = {
		leader_siege = 1
		land_morale = 0.10
	}
}

aquitaine_ideas = { #HAS TO BE BALANCED
	start = {
	}
	bonus = {
	}
	
	free = yes	# Will be added at load.
	
	trigger = {
		OR = {
			primary_culture = auvergnat
			primary_culture = gascon
			primary_culture = occitain
		}
		NOT = { tag = BOU }
	}
	
	authoritarian_trait = {
	}
	garonne_river_trait = {
		advisor_cost = -0.02
	}
	lenga_d_oc = {
		improve_relation_modifier = 0.15 #7 points
		diplomatic_upkeep = 1 # 7 points
	}
	old_kingdoms = {
		legitimacy = 0.15 #7.5
		diplomatic_reputation = 0.5 #8
	}
	two_seas = {
		global_prov_trade_power_modifier = 0.10
		merchants = 1 #20
	}
	languedocien_tolerance = {
		tolerance_heretic = 1 #14
		tolerance_heathen = 1
	}
	gascon_troops = {
		infantry_power = 0.05 # 15
		mercenary_discipline = 0.025 #10
	}
}

shan_ideas = {
	start = {
		heir_chance = 0.25
		num_accepted_cultures = 1 # accepted_culture_threshold = -0.10
	}
	
	bonus = {
		manpower_recovery_speed = 0.10
	}
	
	trigger = {
		primary_culture = shan
		always = no
	}
	
	free = yes
	
	shan_fortified_cities = {
		defensiveness = 0.15
	}
	shan_wet_rice = {
		production_efficiency = 0.10
	}
	sao_pha = {
		global_unrest = -1
	}
	shan_control_of_the_gem_trade = {
		global_own_trade_power = 0.1
	}
	shan_raiders = {
		leader_land_shock = 1
	}
	shan_chronicles = {
		legitimacy = 1
	}
	shan_expansionism = {
		global_manpower_modifier = 0.10
	}
}

central_indic_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = central_indian
	}
	
	free = yes
	
	communism_trait = {
	}
	central_indian_idea = {
		adm_tech_cost_modifier = -0.025
	}
	securing_defenses_central_indic = {
		defensiveness = 0.10 #9
		enemy_core_creation = 0.25
	}
	tribal_religion_central_indic = {
		religious_unity = 0.05 #12
		global_unrest = -1
	}
	tigers_and_elephants_central_indic = {
		hostile_attrition = 1.0 #14
	}
	flexible_caste_central_indic = {
		num_accepted_cultures = 1 #22
		global_manpower_modifier = 0.05
	}
	ancient_roots_central_indic = {
		legitimacy = 0.10
		global_spy_defence = 0.10 #12
		diplomatic_upkeep = 1 #7
	}
}

vindhyan_ideas = {
	start = {
	}
	
	bonus = {
	}
	trigger = {
		OR = {
			tag = BND
			tag = BGL
			AND = {
				tag = GWA
				NOT = { primary_culture = marathi }
			}
		}
	}
	
	free = yes
	
	communism_trait = {
	}
	north_indian_idea = {
	}
	bundelkhandi_rajput_connections = {
		diplomatic_reputation = 1
	}
	forts_of_bundelkhand = {
		defensiveness = 0.15
		fort_maintenance_modifier = -0.15
	}
	bundelkhandi_sacred_land = {
		tolerance_own = 1
	}
	bundeli_kalam = {
		prestige = 0.25
	}
	untamed_bundelkhand = {
		hostile_attrition = 1
	}
	bundelkhandi_diamonds = { #Panna, etc
		trade_efficiency = 0.05
	}
	candella_legacy = {
		legitimacy = 1
	}
}

caucasus_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			culture_group = caucasus_group
			primary_culture = azerbadjani
		}
	}
	
	free = yes
	
	endogamy_trait = {
	}
	beylik_architecture = {
		build_cost = -0.05
		prestige = 0.05
	}
	crossroads_of_cultures = {
		num_accepted_cultures = 1
	}
	alan_mercenaries = {
		possible_mercenaries = 0.25
		mercenary_cost = -0.05
	}
	caucasus_hit_and_run = {
		hostile_attrition = 1
		cavalry_power = 0.025
	}
	persian_poetry = {
		prestige = 0.15
		diplomatic_reputation = 0.25
		trade_range_modifier = 0.25
	}
	eastern_influence = {
		mil_tech_cost_modifier = -0.05
		fort_maintenance_modifier = -0.10
	}
}

kurdish_ideas = {
	start = {
		diplomats = 1
		defensiveness = 0.15
	}
	
	bonus = {
		free_leader_pool = 1
	}
	
	trigger = {
		primary_culture = kurdish
		always = no
	}
	
	free = yes
	
	kurdish_warriors = {
		hostile_attrition = 1
	}
	li_gora_gawiri_kurd_misilman = {
		tolerance_heretic = 2
	}
	kurdish_tribes = {
		global_manpower_modifier = 0.1
	}
	kurdish_diplomacy = {
		diplomatic_upkeep = 1
	}
	kurdish_mercenaries = {
		mercenary_cost = -0.2
	}
	legacy_of_the_kurdish_dynasties = {
		legitimacy = 1
	}
	gorani_literature = {
		prestige_decay = -0.005
	}
}

west_african_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		OR = {
			culture_group = chadic_group
			culture_group = mande_group
			culture_group = senegambian
			culture_group = senufo_group
			culture_group = akan_group
			culture_group = volta_group
			culture_group = saharan_group
			culture_group = plateau_group
			culture_group = kwa_group
		}
	}
	
	free = yes
	
	african_trait = {
	}
	savannah_warriors = {
		land_attrition = -0.04
		movement_speed = -0.02
	}
	trans_saharan_trade = {
		caravan_power = 0.15 #15
		global_foreign_trade_power = 0.1 #7
		trade_range_modifier = 0.25 #5
	}
	west_african_idea_1 = {
		advisor_pool = 1
		advisor_cost = -0.05 #12
	}
	west_african_idea_3 = {
		num_accepted_cultures = 1 #22
	}
	west_african_idea_4 = {
		religious_unity = 0.10 #24
	}
	west_african_idea_6 = {
		trade_efficiency = 0.05 #5
		global_prov_trade_power_modifier = 0.05 #5
		global_regiment_recruit_speed = -0.10 #6
		envoy_travel_time = -0.10
	}
}

sumatran_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		capital_scope = { region = sumatra_region }
		NOT = { tag = SUV }
		#religion_group = muslim
	}
	
	free = yes
	
	anomist_trait = {
	}
	sumatran_idea = {
		defensiveness = 0.025
	}
	indian_ocean_trade = {
		trade_efficiency = 0.10 #6
		trade_range_modifier = 0.25 #5
		global_foreign_trade_power = 0.1 #7
	}
	sufi_legacy = {
		tolerance_heathen = 1 #14
	}
	center_of_scholarship = {
		idea_cost = -0.05 #10
		diplomatic_upkeep = 1 #7
	}
	spice_pirates = {
		galley_power = 0.05 #7
		naval_morale = 0.05
		global_ship_trade_power = 0.10 #8
		privateer_efficiency = 0.10
		global_sailors_modifier = 0.1 #9
	}
	legacy_of_srivijaya = {
		prestige = 0.15 #4
		stability_cost_modifier = -0.05 #6
		legitimacy = 0.10
	}
}

persian_group_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = persian_group
	}
	
	free = yes
	
	endogamy_trait = {
	}
	persian_plateau_idea = {
		defensiveness = 0.025
	}
	persian_language_idea = {
		diplomatic_reputation = 0.5
		prestige = 0.10
		global_institution_spread = 0.05
		embracement_cost = -0.05
	}
	cavalry_and_bowmen = {
		cavalry_power = 0.05
		land_attrition = -0.025
		army_tradition_from_battle = 0.25
	}
	kings_of_kings = {
		vassal_income = 0.10
		adm_tech_cost_modifier = -0.05
	}
	persian_architecture_pg = {
		production_efficiency = 0.10
		build_cost = -0.05
		global_unrest = -0.5
	}
	improved_silk_road = {
		global_prov_trade_power_modifier = 0.05
		caravan_power = 0.1
	}
}

coptic_ideas = {
	start = { #Same as Ethiopia
	}
	
	bonus = {
	}
	
	trigger = {
		religion = coptic
	}
	
	free = yes	# Will be added at load.
	
	equality_trait = { #25
	}
	ethiopia_land_trait = {
		fort_maintenance_modifier = -0.025
	}
	desert_lands = {
		hostile_attrition = 1
		defensiveness = 0.10
	}
	christian_remoteness = {
		tolerance_own = 1
		global_missionary_strength = 0.01
	}
	free_coptics = {
		enemy_core_creation = 0.25
	}
	desert_roads = {
		global_tax_modifier = 0.05
	}
	infidel_sea = {
		global_spy_defence = 0.20
		war_exhaustion = -0.01
	}
}

bodo_kachari_ideas = {
	start = {
		global_manpower_modifier = 0.05
		manpower_recovery_speed = 0.10
	}
	
	bonus = {
		diplomatic_reputation = 2
	}
	
	trigger = {
		OR = {
			primary_culture = meitei
			primary_culture = sutiya
			primary_culture = kochrajbongshi
			primary_culture = tripuri
			primary_culture = naga
			primary_culture = dimasa
		}
		OR = {
			religion = hinduism
			religion = adi_dharam
		}
		always = no
	}
	
	free = yes
	
	maintain_indepence = {
		enemy_core_creation = 0.5
	}
	bihu = { #localize
		global_unrest = -1
	}
	wet_rice_cultivation = {
		production_efficiency = 0.10
	}
	river_warfare = {
		hostile_attrition = 1.0
		defensiveness = 0.25
	}
	tribal_infantry = { #localize
		infantry_power = 0.1
	}
	honor_our_ancestors = { #localize
		tolerance_heretic = 1
		tolerance_heathen = 1
	}
	ahom_land_survey = {
		global_tax_modifier = 0.10
	}
}

pahari_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		primary_culture = pahari
		religion = hinduism
	}
	
	free = yes
	
	anomist_trait = {
	}
	nepal_idea = {
		hostile_attrition = 0.25
	}
	shadows_of_himalayas = { #localize
		defensiveness = 0.10
		enemy_core_creation = 0.33
	}
	pahari_craftsmen = { #localize
		production_efficiency = 0.1
		prestige_decay = -0.005
	}
	rajput_kingdoms = { #localize
		infantry_power = 0.05
		army_tradition_decay = -0.005
	}
	convergence_of_faiths = { #localize
		tolerance_heathen = 1
	}
	old_dynasties = { #localize
		legitimacy = 0.10
		prestige = 0.10
		diplomatic_reputation = 0.5
	}
}

swahili_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = swahili_group
	}
	
	free = yes
	
	african_trait = {
	}
	east_africa_idea = {
		idea_cost = -0.02
	}
	swa_language = {
		diplomatic_reputation = 0.5
		num_accepted_cultures = 1
	}
	swa_indian_ocean_trade = {
		light_ship_cost = -0.05
		naval_forcelimit_modifier = 0.1
		global_sailors_modifier = 0.1
	}
	swa_shirazi_idea = {
		prestige_per_development_from_conversion = 0.25
		tolerance_own = 1
	}
	swa_trade_with_india = {
		global_foreign_trade_power = 0.1
		dip_tech_cost_modifier = -0.05
		trade_range_modifier = 0.25
	}
	swa_trade_empire = {
		global_unrest = -1
		stability_cost_modifier = -0.05
	}
}

russian_ideas = {
	start = {
	}
	
	bonus = {
	}
	
	trigger = {
		culture_group = east_slavic
	}
	
	free = yes
	
	communism_trait = { #25
	}
	cavalry_riders_trait = {
		cav_to_inf_ratio = 0.10
		cavalry_flanking = 0.15
		mil_tech_cost_modifier = 0.05
	}
	russkaya_pravda = { #21
		stability_cost_modifier = -0.05
		global_unrest = -2
		core_creation = -0.10
	}
	fur_trade_idea = {
		global_prov_trade_power_modifier = 0.10
		production_efficiency = 0.10
		caravan_power = 0.025
	}
	alexander_nevsky_idea = {
		prestige_from_land = 0.5
		global_manpower_modifier = 0.10
	}
	outside_alliances_russian_idea = {
		diplomatic_upkeep = 1
		envoy_travel_time = -0.10
		improve_relation_modifier = 0.1
	}
	leadership_mongol_yoke = {
		land_morale = 0.05
		manpower_recovery_speed = 0.05
	}
}
