# Regular Cavalry (32)

unit_type = north_american
type = cavalry

maneuver = 2
offensive_morale = 3
defensive_morale = 4
offensive_fire =   5
defensive_fire =   3
offensive_shock =  4
defensive_shock =  3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}