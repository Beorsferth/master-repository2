# African Hussar

type = cavalry
unit_type = sub_saharan

maneuver = 2
offensive_morale = 3
defensive_morale = 3
offensive_fire = 1
defensive_fire = 0
offensive_shock = 3
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
