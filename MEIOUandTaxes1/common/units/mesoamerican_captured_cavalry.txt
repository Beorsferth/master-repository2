#Captured Horses (13)

unit_type = mesoamerican
type = cavalry

maneuver = 1
offensive_morale = 2
defensive_morale = 5
offensive_fire = 0
defensive_fire = 1
offensive_shock = 3
defensive_shock = 2

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}