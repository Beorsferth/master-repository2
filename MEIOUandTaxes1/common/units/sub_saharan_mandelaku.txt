# Mandelaku Cavalry

type = cavalry
unit_type = sub_saharan

maneuver = 2
offensive_morale = 1
defensive_morale = 1
offensive_fire = 0
defensive_fire = 0
offensive_shock = 1
defensive_shock = 1

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}
