# Kapikulu Cavalry

unit_type = turkishtech
type = cavalry
maneuver = 2

#Tech 20
offensive_morale = 4
defensive_morale = 5
offensive_fire = 0
defensive_fire = 0
offensive_shock = 4
defensive_shock = 3

trigger = {
	NOT = { has_country_flag = raised_special_units }
	NOT = { has_country_flag = no_cavalry }
}