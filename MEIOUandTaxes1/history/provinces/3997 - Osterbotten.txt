# 3997 - Osterbotten

capital = "Oulu"
trade_goods = fur
culture = sapmi
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_hostileness = 1

native_size = 5
native_ferocity = 2

discovered_by = eastern

450.1.1 = {
	set_province_flag = tribals_control_province
	set_province_flag = sami_province
	set_variable = { which = tribals_ratio value = 64 }
#	set_variable = { which = Birkarl_Settlers value = 1 }
#	set_variable = { which = Pomor_Settlers value = 1 }
#	set_variable = { which = Norse_Settlers value = 1 }
#	set_variable = { which = Sami_Natives value = 100 }
}
1356.1.1 = {
	discovered_by = NOR
	discovered_by = SWE
	discovered_by = DEN
	discovered_by = DAN
	add_permanent_province_modifier = {
		name = "6000_population"
		duration = -1
	}
}
1400.1.1 = {
	remove_province_modifier = "6000_population"
	add_permanent_province_modifier = {
		name = "4000_population"
		duration = -1
	}
}
1550.1.1 = {
	remove_province_modifier = "4000_population"
	add_permanent_province_modifier = {
		name = "9000_population"
		duration = -1
	}
}
1640.1.1 = {
	owner = SWE
	controller = SWE
	add_core = SWE
	citysize = 200
	trade_goods = fur
	set_province_flag = trade_good_set
} # The border vs Norway was set earlier but at this point colonialism had also started
1640.1.2 = {
	culture = swedish
	religion = protestant
}
1650.1.1 = {
	trade_goods = iron
	citysize = 320
}
1652.1.1 = {
	capital = "Kiruna"
}
1700.1.1 = {
	citysize = 430
}
1740.1.1 = {
	fort_14th = yes
}
1750.1.1 = {
	citysize = 670
}
1800.1.1 = {
	fort_14th = yes
	citysize = 800
}
1809.3.29 = {
	controller = RUS
}
1809.9.17 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
	remove_core = SWE
} # Treaty of Fredrikshamn
