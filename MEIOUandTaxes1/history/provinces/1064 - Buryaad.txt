# 1064 - Kuznetsk

capital = "Ilimsk"
trade_goods = unknown
culture = evenki
religion = tengri_pagan_reformed

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 1
native_hostileness = 3

discovered_by = steppestech

450.1.1 = {
	set_province_flag = tribals_control_province
}
1610.1.1 = {
	discovered_by = RUS
}
1610.1.1 = {
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	citysize = 500
	trade_goods = fur
	set_province_flag = trade_good_set
} # Part of Russia
1781.1.1 = {
	add_core = RUS
	is_city = yes
}
