# 49 - Brunswiek

owner = BRU
controller = BRU
add_core = BRU

capital = "Brunswiek"
trade_goods = wheat
culture = eastphalian
religion = catholic

hre = yes

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
marketplace = yes
town_hall = yes
local_fortification_1 = yes
warehouse = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 8
	base_production = 2
	base_manpower = 1
}
1529.1.1 = {
	religion = protestant
}
1600.1.1 = {
	fort_14th = yes
}
1737.1.1 = {
	early_modern_university = yes
} # The university of Göttingen was founded in 1737 by George II August, King of Great Britain and prince-elector of Hanover.
1805.12.15 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
} # Treaty of Schoenbrunn, ceded to Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1813.10.13 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = WES
} # The kingdom is dissolved
