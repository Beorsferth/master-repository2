# 1021 - Tsushima
# GG/LS - Japanese Civil War

owner = SOO
controller = SOO
add_core = SOO

capital = "Idufara"
trade_goods = fish
culture = kyushu
religion = mahayana #shinbutsu

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes

discovered_by = chinese

1501.1.1 = {
	base_tax = 4
}
1542.1.1 = {
	discovered_by = POR
}
1590.1.1 = { } # Preparation of the invasion of Korea
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
