# 4115 - Taghaza

capital = "Taghaza"
trade_goods = palm_date # mined salt as well
culture = hassaniya
religion = sunni

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 60
native_ferocity = 4.5
native_hostileness = 9

discovered_by = muslim
discovered_by = soudantech
discovered_by = sub_saharan
# NOT = { discovered_by = SLL }
# NOT = { discovered_by = DSL }

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 88 }
	add_permanent_province_modifier = {
		name = oasis_route
		duration = -1
	}
}
1204.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1518.1.1 = {
	owner = SON
	controller = SON
	add_core = SON
	is_city = yes
}
1591.1.1 = {
	owner = MOR
	controller = MOR
	add_core = MOR
}
1603.1.1 = {
	unrest = 5
} # The death of the Saadita Ahmad I al-Mansur
1604.1.1 = {
	unrest = 0
}
1631.1.1 = {
	owner = TFL
	controller = TFL
}
1668.8.2 = {
	owner = MOR
	controller = MOR
}
