#46 - Mecklenburg

owner = MKL
controller = MKL
add_core = MKL

capital = "Wismar"
trade_goods = wheat
culture = pommeranian
religion = catholic

hre = yes

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
marketplace = yes
town_hall = yes
harbour_infrastructure_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

#500.1.1 = {
#	add_permanent_province_modifier = {
#		name = urban_goods_beer
#		duration = -1
#	}
#}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 8
	base_production = 2
	base_manpower = 1
}
1525.1.1 = {
	fort_14th = yes
}
1530.1.1 = {
	religion = protestant
}
1650.1.1 = {
	fort_14th = no
	fort_17th = yes
}
1750.1.1 = {
	fort_17th = no
	fort_18th = yes
} #add customs house to reflect rapid economic growth
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
