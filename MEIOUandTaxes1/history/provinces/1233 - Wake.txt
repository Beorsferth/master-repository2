# 1233 - Wake

capital = "Saipan"
trade_goods = unknown # fish
culture = chamorro
religion = polynesian_religion

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 20
native_ferocity = 4
native_hostileness = 7

1356.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = remote_island
		duration = -1
	}
}
1568.1.1 = {
	discovered_by = SPA
} # Discovered by �lvaro de Menda�a de Neyra
1796.1.1 = {
	discovered_by = GBR
} # Visited by William Wake
