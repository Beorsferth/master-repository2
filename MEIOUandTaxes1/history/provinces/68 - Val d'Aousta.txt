#68 - V�l d'ao�ta

owner = SAV
controller = SAV
add_core = SAV

capital = "Aousta"
trade_goods = livestock
culture = arpitan
religion = catholic

hre = yes

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1250.1.1 = {
	set_province_flag = freeholders_control_province
}
1530.2.27 = {
	hre = no
	fort_14th = yes
}
1536.4.1 = {
	controller = FRA
	add_core = FRA
}
1538.6.17 = {
	controller = SAV
}
1559.1.1 = {
	remove_core = FRA
}
1618.1.1 = {
	hre = no
}
1691.1.1 = {
	controller = FRA
}
1692.1.1 = {
	controller = SAV
}
1704.1.1 = {
	controller = FRA
}
1706.1.1 = {
	controller = SAV
}
1713.4.12 = {
	owner = SIC
	controller = SIC
	add_core = SIC
} # Treaty of Utrecht
1718.8.2 = {
	owner = SPI
	controller = SPI
	add_core = SPI
	remove_core = SIC
} # Kingdom of Piedmont-Sardinia
1792.9.21 = {
	controller = FRA
} # Conquered by the French
1796.4.25 = {
	owner = FRA
	add_core = FRA
} # The Republic of Alba
1814.4.11 = {
	owner = SPI
	controller = SPI
	remove_core = FRA
} # Napoleon abdicates and Victor Emmanuel is able to return to Turin
1861.2.18 = {
	owner = ITA
	controller = ITA
	add_core = ITA
}
