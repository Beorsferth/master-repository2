# Blekinge
# MEIOU - Gigau

owner = DEN
controller = DEN
add_core = DEN

capital = "Rotnaeby"
trade_goods = lumber
culture = danish
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1356.1.1 = {
	owner = RSW
	controller = RSW
	add_core = RSW
}
1360.1.1 = {
	owner = DEN
	controller = DEN
	remove_core = RSW
}
1500.1.1 = {
	road_network = yes
}
1523.6.21 = {
	owner = DAN
	controller = DAN
	add_core = DAN
	remove_core = DEN
}
1525.1.1 = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #Prelude to 'Grevefejden'(S�ren Norrby)
1525.5.10 = {
	revolt = { }
	controller = DAN
} #S�ren Norrby is defeated
1534.8.15 = {
	revolt = {
		type = nationalist_rebels
		size = 2
	}
	controller = REB
} #'Grevefejden'(Christofer of Oldenburg)
1536.3.15 = {
	revolt = { }
	controller = DAN
} #Liberated by Sweden
1536.3.15 = {
	religion = protestant
} #Unknown date
1599.1.1 = {
	fort_17th = yes
}
1614.1.1 = {
	capital = "Kristianstad"
}
1658.2.26 = {
	owner = SWE
	controller = SWE
	add_core = SWE
	remove_core = DAN
} #The Peace of Roskilde
1672.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1680.1.1 = {
	capital = "Karlskrona"
}
1722.1.1 = {
	culture = swedish
} #linguicide mostly accomplished
