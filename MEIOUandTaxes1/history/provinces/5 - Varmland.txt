# V�rmland
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Karlstad"
trade_goods = iron
culture = swedish
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1200.1.1 = {
	set_province_flag = freeholders_control_province
}
1500.3.3 = {
	base_tax = 5
	base_production = 0
	base_manpower = 0
}
1512.1.1 = {
	fort_14th = yes
}
1527.6.1 = {
	religion = protestant
}
#controller of trade route to Denmark
#N�rke-V�rmlands regemente
1529.12.17 = {
	merchant_guild = yes
}
#minor court belonging to G�ta Hovr�tt
#Unknown date
1670.1.1 = {
	fort_14th = no
	fort_15th = yes
} #Due to the "Staplepolicy act"
1680.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1730.1.1 = {
	fort_16th = no
	fort_17th = yes
}
#Due to the support of manufactories
