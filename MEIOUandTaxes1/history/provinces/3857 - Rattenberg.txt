# No previous file for Rattenberg

owner = BAW
controller = BAW
add_core = BAW

capital = "Rattenberg"
trade_goods = wheat #glassware
culture = bavarian
religion = catholic

hre = yes

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_glassware
		duration = -1
	}
}
1340.12.20 = {
	owner = BAV
	controller = BAV
	add_core = BAV
	remove_core = BAW
}
1349.1.1 = {
	owner = BAX
	controller = BAX
	add_core = BAX
	remove_core = BAV
}
1392.1.1 = {
	owner = BAW
	controller = BAW
	add_core = BAW
}
1445.4.7 = {
	owner = BAX
	controller = BAX
	remove_core = BAW
}
1500.1.1 = {
	road_network = yes
}
1505.7.30 = {
	controller = HAB
	owner = HAB
	add_core = HAB
	remove_core = BAX
	culture = austrian
} # Diet of Cologne, reward for mediation for the end of the Landshut War of Succession
1520.5.5 = {
	base_tax = 6
	base_production = 0
	base_manpower = 0
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
