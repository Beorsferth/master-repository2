# 3738 - Mansfeld

owner = MFD
controller = MFD
add_core = MFD

capital = "Mansfeld"
trade_goods = wheat
culture = high_saxon
religion = catholic

hre = yes

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1133.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = silver
}
1500.1.1 = {
	road_network = yes
}
1540.1.1 = {
	religion = protestant
}
1780.1.1 = {
	owner = PRU
	controller = PRU
	add_core = PRU
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
