# 1113 - Beyla

capital = "Beyla"
trade_goods = livestock
culture = susu
religion = west_african_pagan_reformed

hre = no

base_tax = 14
base_production = 0
base_manpower = 0

native_size = 75
native_ferocity = 6
native_hostileness = 7

discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 84 }
}
1520.1.1 = {
	base_tax = 17
}
1530.1.1 = {
	owner = MNE
	controller = MNE
	add_core = MNE
	is_city = yes
	trade_goods = iron
}
