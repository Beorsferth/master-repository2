#69 - Breisgau
# Freiburg

owner = FRE
controller = FRE
add_core = FRE

capital = "Freiburg"
trade_goods = wine
culture = rhine_alemanisch
religion = catholic

hre = yes

base_tax = 7
base_production = 1
base_manpower = 0

is_city = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = freeholders_control_province
}
1368.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = FRE
}
1379.1.1 = {
	controller = TIR
	owner = TIR
	add_core = TIR
}
1457.1.1 = {
	small_university = yes
}
1490.1.1 = {
	controller = HAB
	owner = HAB
	add_core = HAB
	remove_core = TIR
}

# Freiburger M�nster
#Freiburg has one of Germany's oldest Universities (founded 1457)
1500.1.1 = {
	road_network = yes
}
1510.1.1 = {
	fort_14th = yes
} #Breisach is one of the most important Fortresses on the upper Rhine
1520.5.5 = {
	base_tax = 8
}
1524.4.1 = {
	unrest = 8
} # Peasant Revolts - 18.000 peasants take Freiburg (May)
1524.12.1 = {
	unrest = 0
}
1530.1.5 = {
	controller = FRE
	owner = FRE
	add_core = FRE
	remove_core = HAB
}
1646.1.1 = {
	fort_14th = yes
}
1797.10.17 = {
	owner = MOD
	controller = MOD
	add_core = MOD
	remove_core = HAB
} # Treaty of Campo Formio
1805.12.26 = {
	owner = BAD
	controller = BAD
	add_core = BAD
	remove_core = MOD
} # Treaty of Pressburg
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
