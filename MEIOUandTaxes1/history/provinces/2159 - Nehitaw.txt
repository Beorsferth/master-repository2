# 2159 - Dortmund

owner = FRD
controller = FRD
add_core = FRD

capital = "Duorpm"
trade_goods = wheat # linen
culture = westphalian
religion = catholic

hre = yes

base_tax = 2
base_production = 1
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes
workshop = yes
marketplace = yes
town_hall = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_steel
		duration = -1
	}
}
1500.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 3
	base_production = 1
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
