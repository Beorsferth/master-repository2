# 3736 - Coburg

owner = THU
controller = THU
add_core = THU

capital = "Coburg"
trade_goods = wheat
culture = eastfranconian
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
road_network = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1440.1.1 = {
	owner = SAX
	controller = SAX
	add_core = SAX
	remove_core = MEI
}#Duchy of Thuringia is inherited by Saxony
1500.1.1 = {
	road_network = yes
	fort_14th = yes
}
1530.1.1 = {
	religion = protestant
}
1546.1.1 = {
	fort_14th = yes
}
1547.5.19 = {
	owner = THU
	controller = THU
	add_core = THU
	remove_core = SAX
}
1596.1.1 = {
	owner = SCB
	controller = SCB
	add_core = SCB
	remove_core = THU
}
1633.1.1 = {
	owner = SWR
	controller = SWR
	add_core = SWR
}
1680.1.1 = {
	owner = SCB
	controller = SCB
	add_core = SCB
	remove_core = SWR
}
