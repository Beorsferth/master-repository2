#

owner = BRE
controller = BRE
add_core = BRE

capital = "Stade"
trade_goods = fish
culture = old_saxon
religion = catholic

hre = yes

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

1500.1.1 = {
	road_network = yes
}
1510.1.1 = {
	fort_14th = yes
}
1520.5.5 = {
	base_tax = 7
}
1529.1.1 = {
	religion = protestant
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1648.10.24 = {
	owner = SWE
	controller = SWE
	add_core = SWE
} # Swedish dominion, Peace of Westphalia
1650.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1719.9.30 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = SWE
} # The Treaty of Stockholm
1803.7.5 = {
	controller = FRA
} # French control
1805.12.15 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = HAN
} # Treaty of Schoenbrunn, ceded to Prussia
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
1807.7.9 = {
	owner = WES
	controller = WES
	add_core = WES
	remove_core = PRU
} # The Second Treaty of Tilsit, the kingdom of Westfalia
1810.12.13 = {
	owner = FRA
	controller = FRA
	add_core = FRA
	remove_core = WES
} # Annexed by France
1814.4.11 = {
	owner = HAN
	controller = HAN
	add_core = HAN
	remove_core = FRA
} # Napoleon abdicated unconditionally
