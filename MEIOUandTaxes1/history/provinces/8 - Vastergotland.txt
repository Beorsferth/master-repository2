# Skaraborg
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Skara" #"Domkyrkan i Skara"
trade_goods = wheat
culture = swedish
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
temple = yes
local_fortification_1 = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_estuary
	add_permanent_province_modifier = {
		name = "vastergotland_natural_harbour"
		duration = -1
	}
}
1500.3.3 = {
	base_tax = 9
	base_production = 0
	base_manpower = 1
}
1527.6.1 = {
	religion = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
1580.1.1 = {
	fort_14th = yes
} #Skaraborg Castle
1598.8.15 = {
	controller = PLC
} #Sigismund tries to reconquer his crown
1598.12.15 = {
	controller = SWE
} #Duke Karl get it back
1601.1.1 = {
	fort_14th = no
	fort_15th = yes
} #New �lvsborg Castle
1612.5.24 = {
	controller = DAN
} #The War of Kalmar-Captured by Christian IV
1613.1.20 = {
	controller = SWE
}#The Peace of Kn?red
1621.1.1 = {
	capital = "Goteborg"
}

#V?stg?tsdals regemente/V?stg?ta regemente till H?st
#minor court belonging to G?ta Hovr?tt
1740.1.1 = {
	fort_15th = no
	fort_16th = yes
}
1780.1.1 = {
	fort_16th = no
	fort_17th = yes
}
