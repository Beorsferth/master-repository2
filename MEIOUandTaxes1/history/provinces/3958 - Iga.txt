# 3958 - Iga

owner = NIK
controller = NIK
add_core = NIK

capital = "Uyeno"
trade_goods = rice
culture = kansai
religion = mahayana

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

is_city = yes
temple = yes

discovered_by = chinese

1433.1.1 = {
	controller = YMN
	owner = YMN
}
1435.1.1 = {
	owner = NIK
	controller = NIK
}
1501.1.1 = {
	base_tax = 6
}
1542.1.1 = {
	discovered_by = POR
}
1572.1.1 = {
	owner = ODA
	controller = ODA
}
1583.1.1 = {
	owner = TGW
	controller = TGW
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
