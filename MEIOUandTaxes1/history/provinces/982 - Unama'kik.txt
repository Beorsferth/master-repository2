# No previous file for Unama'kik

capital = "Unama'kik"
trade_goods = unknown
culture = miqmaq
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}
1497.6.24 = {
	discovered_by = ENG
} # John Cabbot
1534.1.1 = {
	discovered_by = FRA
} # Jacques Cartier
1629.1.1 = {
	owner = FRA
	controller = FRA
	citysize = 200
	capital = "Fort Sainte Anne"
	religion = catholic
	culture = francien
	trade_goods = fur
	set_province_flag = trade_good_set
}
1650.1.1 = {
	add_core = FRA
}
1719.1.1 = {
	capital = "Louisbourg"
	citysize = 100
}
1750.1.1 = {
	add_core = QUE
	culture = canadian
}
1763.2.10 = {
	owner = GBR
	controller = GBR
	add_core = GBR
	remove_core = FRA
	religion = protestant #anglican
	culture = american
}
