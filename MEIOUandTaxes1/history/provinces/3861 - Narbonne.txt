# 3861 - Narbona

owner = FRA
controller = FRA
capital = "Narbona"
trade_goods = wine
culture = occitain
religion = catholic
base_tax = 3
base_production = 1
base_manpower = 0
is_city = yes
local_fortification_1 = yes
road_network = yes
town_hall = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_cloth
		duration = -1
	}
	set_province_flag = mined_goods
	set_province_flag = salt
}
1204.6.15 = {
	owner = ARA
	controller = ARA
	add_core = ARA
}
1276.1.1 = {
	owner = BLE
	controller = BLE
	add_core = BLE
}
1349.1.1 = {
	owner = FRA
	controller = FRA
	remove_core = ARA
	add_claim = ARA
}
1356.1.1 = {
	add_core = FRA
	add_core = TOU
}
1422.1.1 = {
	remove_core = BLE
}
1422.10.21 = {
	owner = DAU
	controller = DAU
	add_core = DAU
}
1429.7.17 = {
	owner = FRA
	controller = FRA
	remove_core = DAU
}
1494.1.1 = {
	remove_claim = ARA
}
1520.5.5 = {
	base_tax = 5
}
1530.1.2 = {
	road_network = no
	paved_road_network = yes
}
1545.1.1 = {
	fort_14th = yes
}
1560.1.1 = {
	religion = reformed
}
1622.1.17 = {
	religion = catholic
}
