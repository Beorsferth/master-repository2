# 47 - Vorpommern-Stettin

owner = POM
controller = POM
add_core = POM

capital = "Stettin"
trade_goods = lumber
culture = pommeranian
religion = catholic

hre = yes

base_tax = 6
base_production = 1
base_manpower = 0

is_city = yes
harbour_infrastructure_2 = yes
marketplace = yes
town_hall = yes
local_fortification_1 = yes
road_network = yes
temple = yes

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_estuary
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = oder_estuary_modifier
		duration = -1
	}
}
1295.1.1 = {
	owner = PST
	controller = PST
	add_core = PST
	remove_core = POM
}
1478.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PST
} # Duchy reunited for a short period
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 6
	base_production = 2
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
#1529.1.1 = {
#	add_core = BRA
#}
1531.1.1 = {
	owner = PST
	controller = PST
	add_core = PST
	remove_core = POM
} # Fifth Partition
1534.1.1 = {
	religion = protestant
	fort_14th = yes
}
1625.1.1 = {
	owner = POM
	controller = POM
	add_core = POM
	remove_core = PST
} # Final reunification
1630.7.10 = {
	owner = SWE
	controller = SWE
	add_core = SWE
} # Treaty of Stettin
1720.6.3 = {
	owner = PRU
	controller = PRU
	add_core = PRU
	remove_core = SWE
} # Treaties of Stockholm
1806.7.12 = {
	hre = no
} # The Holy Roman Empire is dissolved
