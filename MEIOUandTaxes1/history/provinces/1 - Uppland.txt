# 1 - Uppland
# MEIOU - Gigau

owner = SWE
controller = SWE
add_core = SWE

capital = "Stockholm"
trade_goods = iron
culture = swedish
religion = catholic

hre = no

base_tax = 7
base_production = 0
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes
marketplace = yes
temple = yes
local_fortification_1 = yes

#"Stockholms borgarskap"

discovered_by = eastern
discovered_by = western
discovered_by = muslim

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "uppland_large_natural_harbor"
		duration = -1
	}
}
1356.1.1 = {
	owner = RSW
	controller = RSW
	add_core = RSW
}
1360.1.1 = {
	owner = SWE
	controller = SWE
	remove_core = RSW
}
1400.1.1 = {
	fort_14th = yes
}
1477.1.1 = {
	small_university = yes
}
1500.3.3 = {
	base_tax = 9
	base_production = 1
	base_manpower = 1
}
1522.2.15 = {
	shipyard = yes
}
1522.3.20 = {
	naval_arsenal = yes
}
1527.6.1 = {
	religion = protestant
	#reformation_center = protestant
}
1529.12.17 = {
	merchant_guild = yes
}
1530.1.1 = {
	weapons = yes
}
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
1537.1.1 = {
	fort_14th = yes
} #Gripsholms Castle
1598.8.12 = {
	controller = PLC
} #Sigismund tries to reconquer his crown
1598.12.15 = {
	controller = SWE
} #Duke Karl get it back
1621.1.1 = {
	fort_14th = no
	fort_15th = yes
} # Key forts defending roads to Stockholm
1680.1.1 = {
	fort_15th = no
	fort_16th = yes
} # Added forts giving coastal defense of region
1730.1.1 = {
	fort_16th = no
	fort_17th = yes
} # estimated date
