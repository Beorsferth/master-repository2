# 1262 - Kakisalmi/Kexholm
# MEIOU - Gigau

owner = NOV
controller = NOV
add_core = NOV

capital = "Kakisalmi"
trade_goods = fur
culture = karelian
religion = orthodox

base_tax = 0
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_2 = yes
town_hall = yes #Second largest town in Novgorod.
marketplace = yes

discovered_by = eastern
discovered_by = western

450.1.1 = {
	add_permanent_province_modifier = {
		name = "3000_population"
		duration = -1
	}
}

1400.1.1 = {
	remove_province_modifier = "3000_population"
	add_permanent_province_modifier = {
		name = "2000_population"
		duration = -1
	}
}

1478.1.14 = {
	owner = MOS
	controller = MOS
	add_core = MOS
	remove_core = NOV
}
1547.1.1 = {
	owner = RUS
	controller = RUS
	add_core = RUS
	remove_core = MOS
} # Ivan Grozny becomes the first Tsar of Russia

1550.1.1 = {
	remove_province_modifier = "2000_population"
	base_tax = 4
}

1611.1.1 = {
	controller = SWE
}
1617.2.17 = {
	owner = SWE
	add_core = SWE
	rename_capital = "Kexholm"
	change_province_name = "Kexholm"
} # The Peace of Stolbova
1649.1.1 = {
	fort_14th = yes
}
1710.9.9 = {
	controller = RUS
} # The Great Nordic War-Captured Keksholm
1721.8.30 = {
	owner = RUS
	remove_core = SWE
	rename_capital = "Keksgolm"
	change_province_name = "Keksgolm"
} # The Peace of Nystad
1812.1.1 = {
	owner = FIN
	controller = FIN
	add_core = FIN
	remove_core = RUS
}
1850.1.1 = {
	base_tax = 5
}