# 152 - Zagorje

owner = HUN
controller = HUN
add_core = HUN

capital = "Varazdin"
trade_goods = hemp
culture = croatian
religion = catholic

hre = no

base_tax = 8
base_production = 0
base_manpower = 0

is_city = yes
marketplace = yes
road_network = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

1356.1.1 = {
	add_core = CRO
	add_permanent_province_modifier = {
		name = hungarian_estates
		duration = -1
	}
	add_permanent_province_modifier = {
		name = slavonia_province
		duration = -1
	}
}
1520.5.5 = {
	base_tax = 10
	base_production = 0
	base_manpower = 0
}
1526.8.30 = {
	owner = HAB
	controller = HAB
	add_core = HAB
} # Battle of Moh�cs
1530.1.3 = {
	road_network = no
	paved_road_network = yes
}
#1540.1.1 = {
#	owner = TUR
#	controller = TUR
#	add_core = TUR
#}
#1604.1.1 = {
#	controller = REB
#} # The nobility in Royal Hungary rebelled
#1606.1.1 = {
#	controller = TUR
#}
1687.9.29 = {
	controller = HAB
} # Occupied by the Habsburg Empire
1699.1.1 = {
	owner = HAB
	controller = HAB
	add_core = HAB
	remove_core = TUR
}
1721.1.1 = {
	fort_14th = yes
}
