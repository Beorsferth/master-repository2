# 1062 - Irkutsk

owner = YUA
controller = YUA
add_core = YUA

capital = "Irkutsk"
trade_goods = fur
culture = buryat
religion = tengri_pagan_reformed

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 1
native_hostileness = 3

is_city = yes

discovered_by = steppestech

450.1.1 = {
	add_permanent_province_modifier = {
		name = "irkutsk_confluence"
		duration = -1
	}
	set_province_flag = has_confluence
	set_province_flag = good_natural_place
	set_province_flag = mined_goods
	set_province_flag = salt
	set_province_flag = tribals_control_province
}
1356.1.1 = {
	add_core = BRT
}
1392.1.1 = {
	owner = BRT
	controller = BRT
	remove_core = YUA
}
1632.1.1 = {
	discovered_by = RUS
}
1632.9.25 = {
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	is_city = yes
	trade_goods = fur
	set_province_flag = trade_good_set
}
1657.1.1 = {
	add_core = RUS
}
