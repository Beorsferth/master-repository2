# 3971 - Chikugo

owner = KKC
controller = KKC
add_core = KKC

capital = "Kurume"
trade_goods = rice
culture = kyushu
religion = mahayana

hre = no

base_tax = 12
base_production = 0
base_manpower = 1

is_city = yes
local_fortification_1 = yes

discovered_by = chinese

1356.1.1 = {
	add_core = UTN
}
1363.1.1 = {
	add_core = OTM
	owner = OTM
	controller = OTM
}
1375.1.1 = {
	add_core = IGW
	owner = IGW
	controller = IGW
}
1391.1.1 = {
	add_core = OTM
	owner = OTM
	controller = OTM
}
1427.1.1 = {
	owner = KKC
	controller = KKC
}
1469.1.1 = {
	owner = OTM
	controller = OTM
}
1501.1.1 = {
	base_tax = 21
	base_manpower = 2
}
1542.1.1 = {
	discovered_by = POR
}
1578.1.1 = {
	owner = SMZ
	controller = SMZ
}
1585.1.1 = {
	religion = catholic
}
1615.1.1 = {
	religion = mahayana
}
1650.1.1 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
