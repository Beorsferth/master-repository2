# 1033 - Wakasa
# GG/LS - Japanese Civil War

owner = ISK
controller = ISK
add_core = ISK

capital = "Miyazu"
trade_goods = fish
culture = kansai
religion = mahayana #shinbutsu

hre = no

base_tax = 5
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
temple = yes # Wakasahime Shirine (Obama) and Kehi Shirine (Tsuruga).

discovered_by = chinese

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "tajima_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = NIK
}
1501.1.1 = {
	base_tax = 9
}
1525.1.1 = {
	owner = ODA
	controller = ODA
}
1542.1.1 = {
	discovered_by = POR
}
1615.6.4 = {
	owner = JAP
	controller = JAP
	add_core = JAP
}
