# 2759 - Bono

owner = BON
controller = BON
add_core = BON

capital = "Begho"
trade_goods = slaves
culture = akaa
religion = west_african_pagan_reformed

hre = no

base_tax = 7
base_production = 0
base_manpower = 0

discovered_by = soudantech
discovered_by = sub_saharan

450.1.1 = {
	set_province_flag = tribals_control_province
	set_variable = { which = tribals_ratio	value = 71 }
	set_province_flag = mined_goods
	set_province_flag = gold
}