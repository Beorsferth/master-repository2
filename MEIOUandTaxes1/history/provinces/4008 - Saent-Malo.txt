# 4008 - Saent-Malo

owner = BRI
controller = BRI
add_core = BRI

capital = "Saint Malo"
trade_goods = fish
culture = breton
religion = catholic

hre = no

base_tax = 6
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_2 = yes
temple = yes
harbour_infrastructure_1 = yes
fort_14th = yes
workshop = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

450.1.1 = {
	add_permanent_province_modifier = {
		name = "armor_natural_harbour"
		duration = -1
	}
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
}
1341.4.30 = {
	owner = MNF
	controller = MNF
	add_core = MNF
	add_core = BLO
	remove_core = BRI
	add_permanent_province_modifier = {
		name = saint_malo_city1
		duration = -1
	}
	set_local_autonomy = 75
} # Jean III de Bretagne dies in Caen
1365.4.12 = {
	owner = BRI
	controller = BRI
	add_core = BRI
	remove_core = BLO
	remove_core = MNF
} # End of the Brittany war of succession with the death of Charles de Blois
1378.1.1 = {
	add_core = FRA
}
# Charles V invades Brittany without resistance
1393.1.1 = {
	owner = FRA
	controller = FRA
} # The pope give the city to the crown of France, with the support of the population of Saint-Malo
1415.1.1 = {
	owner = BRI
	controller = BRI
} # The city is retaken by Brittany.
1495.1.1 = {
	add_permanent_province_modifier = {
		name = saint_malo_city2
		duration = -1
	}
	remove_province_modifier = saint_malo_city1
}
1520.5.5 = {
	base_tax = 6
	base_production = 1
	base_manpower = 0
}
1522.3.20 = {
	naval_arsenal = yes
}
1530.8.4 = {
	owner = FRA
	controller = FRA
} # Union Treaty
1588.12.1 = {
	unrest = 5
} # Henri de Guise assassinated at Blois, Ultra-Catholics into a frenzy
1590.3.11 = {
	add_permanent_province_modifier = {
		name = saint_malo_city3
		duration = -1
	}
	remove_province_modifier = saint_malo_city2
}
1594.12.5 = {
	remove_province_modifier = saint_malo_city3
	add_permanent_province_modifier = {
		name = saint_malo_city2
		duration = -1
	}
	unrest = 0
} # 'Paris vaut bien une messe!', Henri converts to Catholicism
