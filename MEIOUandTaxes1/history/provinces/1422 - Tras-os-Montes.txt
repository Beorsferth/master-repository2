# 1422 - Tras-os-Montes

owner = POR
controller = POR
add_core = POR

capital = "Bragan�a"
trade_goods = wine
culture = portugese
religion = catholic

hre = no

base_tax = 10
base_production = 0
base_manpower = 1

is_city = yes
fort_14th = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_silk
		duration = -1
	}
}
1372.5.5 = {
	unrest = 1
} # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5 = {
	unrest = 0
} # Civil unrest repressed.
1420.1.1 = {
	base_tax = 9
}
1500.3.3 = {
	base_tax = 12
	base_production = 1
	base_manpower = 1
}
1580.8.25 = {
	controller = SPA
}
1580.8.26 = {
	controller = POR
}
1640.1.1 = {
	unrest = 8
} # Revolt headed by John of Bragan�a
1640.12.1 = {
	unrest = 0
}
1807.11.30 = {
	controller = FRA
} # Occupied by France
1808.8.30 = {
	controller = POR
}
1810.7.25 = {
	controller = FRA
} # Invaded after the battle of C�a
1811.1.1 = {
	controller = POR
} # The Napoleonic army retreats from Portugal
