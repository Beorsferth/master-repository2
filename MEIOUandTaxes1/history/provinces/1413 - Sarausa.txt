# 1413 - Sarausa

owner = SIC
controller = SIC
add_core = SIC

capital = "Sarausa"
trade_goods = wheat #salt
culture = sicilian
religion = catholic

hre = no

base_tax = 7
base_production = 2
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
urban_infrastructure_1 = yes
harbour_infrastructure_2 = yes
marketplace = yes
road_network = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = has_natural_harbour
	set_province_flag = has_small_natural_harbour
	set_province_flag = good_natural_place
	add_permanent_province_modifier = {
		name = "messina_natural_harbour"
		duration = -1
	}
}
1356.1.1 = {
	add_core = ARA
	add_core = KNP
}
1409.1.1 = {
	owner = ARA
	controller = ARA
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = ARA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becames co-regent of Castille
1520.5.5 = {
	base_tax = 3
	base_production = 8
	base_manpower = 0
}
1530.1.1 = {
	owner = KNP
	controller = KNP
	remove_core = SPA
}
1531.1.1 = {
	owner = SPA
	controller = SPA
	add_core = SPA
}
1706.7.1 = {
	controller = SAV
}
1713.4.11 = {
	owner = SIC
	controller = SIC
	remove_core = SPA
}
1718.8.2 = {
	owner = HAB
	controller = HAB
	add_core = HAB
}
1734.6.2 = {
	owner = NAP
	controller = NAP
	add_core = NAP
	remove_core = HAB
}
#1815.5.3 = {
#	owner = SIC
#	controller = SIC
#	remove_core = NAP
#}
1861.2.18 = {
	owner = ITE
	controller = ITE
	add_core = ITE
}
