# 1044 - Ulia

capital = "Tauisk"
trade_goods = unknown
culture = eveni
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 1
native_hostileness = 3

450.1.1 = {
	set_province_flag = tribals_control_province
}
1647.1.1 = {
	discovered_by = RUS
} # V. Atlasov
1732.1.1 = {
	owner = RUS
	controller = RUS
	citysize = 600
#	religion = orthodox
#	culture = russian
	trade_goods = gold
	set_province_flag = trade_good_set
} # Conquered by Russia
1757.1.1 = {
	add_core = RUS
	is_city = yes
}

