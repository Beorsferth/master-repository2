# 228 - Barlavento Algarvio

owner = POR
controller = POR
add_core = POR

capital = "Faro"
trade_goods = sugar
culture = portugese
religion = catholic

hre = no

base_tax = 4
base_production = 0
base_manpower = 0

is_city = yes
local_fortification_1 = yes
temple = yes
town_hall = yes
harbour_infrastructure_1 = yes

discovered_by = western
discovered_by = muslim
discovered_by = eastern
discovered_by = turkishtech

1372.5.5 = {
	unrest = 2
} # Marriage between King Ferdinand and D. Leonor de Menezes that brought civil unrest and revolt.
1373.5.5 = {
	unrest = 0
} # Civil unrest repressed.
1420.1.1 = {
	base_tax = 1
	base_production = 1
}
1500.3.3 = {
	base_tax = 3
	base_production = 1
}
1522.3.20 = {
	naval_arsenal = yes
}
1640.1.1 = {
	unrest = 8
} # Revolt headed by John of Bragan�a
1640.12.1 = {
	unrest = 0
}
1807.11.30 = {
	controller = SPA
} # Occupied by Spanish troops led by Manuel Godoy

