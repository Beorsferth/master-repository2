# 3367 - Eivissa

owner = ARA
controller = ARA
add_core = ARA

capital = "Eivissa"
trade_goods = fish # Mediterranean trade
culture = catalan # balearic
religion = catholic

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

is_city = yes
harbour_infrastructure_1 = yes
local_fortification_1 = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

450.1.1 = {
	set_province_flag = mined_goods
	set_province_flag = salt
}
1356.1.1 = {
	add_core = BLE
}
1462.1.1 = {
	unrest = 2
} # Remen�a peasant revolt, in parallel with the Catalan civil war.
1472.1.1 = {
	unrest = 0
} # End of the First Remen�a revolt
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	remove_core = BLE
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1521.3.16 = {
	controller = REB
} # The Germanies movement reaches the archipelago, the viceroy is deposed by the revolters
1523.3.8 = {
	controller = SPA
} # The royal army retakes the city of Palma
1705.10.9 = {
	controller = HAB
} # Balearic isles side with the Austrian cause
1713.4.11 = {
	controller = REB
} # The pro-Austrian forces in Mallorca and E�vissa ignore the Treaty of Utrecht.
1713.7.13 = {
	remove_core = ARA
	remove_core = BLE
}
1714.9.14 = {
	controller = SPA
} # Mallorca and E�vissa surrender shortly after the fall of Barcelona
