# 3863 - Ommelanden

owner = FRI
controller = FRI
add_core = FRI

capital = "Grunnen"
trade_goods = livestock
culture = frisian
religion = catholic

hre = yes

base_tax = 3
base_production = 1
base_manpower = 1

is_city = yes
harbour_infrastructure_1 = yes
town_hall = yes
local_fortification_1 = yes
workshop = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim

1040.1.1  = {
	add_permanent_province_modifier = {
		name = "city_of_groningen"
		duration = -1
	}
	set_province_flag = freeholders_control_province
}
1500.1.1 = {
	road_network = yes
}
1520.5.5 = {
	base_tax = 6
	base_production = 3
	base_manpower = 1
}
1522.2.15 = {
	shipyard = yes
}
1536.1.1  = {
	religion = reformed
	#reformation_center = reformed
}
1579.1.23 = {
	add_core = NED
	remove_core = FRI
} # Union of Utrecht - Friesland joins
1660.1.1  = {
	fort_14th = no
	fort_15th = yes
}
1730.1.1  = {
	fort_15th = no
	fort_16th = yes
}
1795.1.1  = {
	owner = NED
	controller = NED
}
1810.7.10 = {
	owner = FRA
	controller = FRA
	add_core = FRA
} # Annexed by France
1813.11.30 = {
	owner = NED
	controller = NED
	remove_core = FRA
} # William returns to the Netherlands
