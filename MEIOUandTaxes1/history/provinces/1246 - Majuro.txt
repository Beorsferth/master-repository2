# 1246 - Aorangi

capital = "Aorangi"
trade_goods = unknown # wool
culture = micronesian
religion = polynesian_religion

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 4
native_hostileness = 9

450.1.1 = {
	set_province_flag = tribals_control_province
	add_permanent_province_modifier = {
		name = remote_island
		duration = -1
	}
}
1642.1.1 = {
	discovered_by = NED
} # Abel Tasman
