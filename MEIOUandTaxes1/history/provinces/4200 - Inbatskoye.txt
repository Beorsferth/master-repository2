# No previous file for Yugh

capital = "Yugh"
trade_goods = unknown
culture = ket
religion = tengri_pagan_reformed

hre = no

base_tax = 1
base_production = 0
base_manpower = 0

native_size = 50
native_ferocity = 2
native_hostileness = 1

450.1.1 = {
	set_province_flag = tribals_control_province
}
1750.1.1 = {
	discovered_by = RUS
	owner = RUS
	controller = RUS
#	religion = orthodox
#	culture = russian
	citysize = 344
	trade_goods = fur
	rename_capital = "Inbatskoye"
	change_province_name = "Inbatskoye"
}
