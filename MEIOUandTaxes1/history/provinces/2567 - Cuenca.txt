# 2567 - Cuenca + Huete

owner = CAS #Juan II of Castille
controller = CAS
add_core = CAS

capital = "Cuenca"
trade_goods = wool # leather
culture = castillian # culture = new_castillian
religion = catholic

hre = no

base_tax = 10
base_production = 1
base_manpower = 0

is_city = yes
road_network = yes
workshop = yes
town_hall = yes
local_fortification_1 = yes
temple = yes

discovered_by = western
discovered_by = eastern
discovered_by = muslim
discovered_by = turkishtech

500.1.1 = {
	add_permanent_province_modifier = {
		name = urban_goods_leather
		duration = -1
	}
}
1356.1.1 = {
	owner = ENR
	controller = ENR
	add_core = ENR
	add_permanent_province_modifier = {
		name = "lordship_of_toledo"
		duration = -1
	}
}
1369.3.23 = {
	remove_core = ENR
	owner = CAS
	controller = CAS
}
1400.1.1 = {
	fort_14th = yes
}
1500.3.3 = {
	base_tax = 9
	base_production = 3
}
1516.1.23 = {
	controller = SPA
	owner = SPA
	add_core = SPA
	road_network = no
	paved_road_network = yes
} # King Fernando dies, Carlos inherits Aragon and becomes co-regent of Castille
1713.4.11 = {
	remove_core = CAS
}
1808.6.6 = {
	controller = REB
}
1811.1.1 = {
	controller = SPA
}
1812.10.1 = {
	controller = REB
}
1813.12.11 = {
	controller = SPA
}
