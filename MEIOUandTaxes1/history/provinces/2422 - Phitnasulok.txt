# 2422 - Phitnasulok

owner = SUK
controller = SUK
add_core = SUK

capital = "Phitsanulok"
trade_goods = rice
culture = thai
religion = buddhism

hre = no

base_tax = 2
base_production = 2
base_manpower = 0

is_city = yes
temple = yes
town_hall = yes
marketplace = yes

discovered_by = chinese
discovered_by = indian

1378.1.1 = {
	owner = AYU
	controller = AYU
	add_core = AYU
	remove_core = SUK
}
1564.2.1 = {
	add_core = TAU
} # Burmese vassal
1584.5.3 = {
	remove_core = TAU
}
1767.4.1 = {
	unrest = 7
} # The fall of Ayutthaya
1767.4.8 = {
	owner = SIA
	controller = SIA
	add_core = SIA
	remove_core = AYU
	unrest = 0
}
