# No previous file for Hohe Nakota

capital = "Lakota"
trade_goods = unknown
culture = nakota
religion = totemism

hre = no

base_tax = 3
base_production = 0
base_manpower = 0

native_size = 15
native_ferocity = 3
native_hostileness = 4

450.1.1 = {
	set_province_flag = tribals_control_province
}
1760.1.1 = {
	owner = ASI
	controller = ASI
	add_core = ASI
	culture = nakota
	trade_goods = fur
	is_city = yes
} # Great Plain tribes spread over vast territories
1792.1.1 = {
	owner = GBR
	controller = GBR
	citysize = 500
	culture = english
	religion = protestant
	trade_goods = fur
} # Fort Dunvegan
1817.1.1 = {
	add_core = GBR
}
