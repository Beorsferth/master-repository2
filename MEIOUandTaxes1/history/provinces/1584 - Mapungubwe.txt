# 1584 - Mapungubwe

capital = "Mapungubwe"
trade_goods = unknown # ivory
culture = xhosa
religion = animism

hre = no

base_tax = 2
base_production = 0
base_manpower = 0

native_size = 10
native_ferocity = 2
native_hostileness = 5

450.1.1 = {
	set_province_flag = tribals_control_province
}