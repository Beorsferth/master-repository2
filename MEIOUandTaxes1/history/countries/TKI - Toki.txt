# TKI - Toki clan

government = japanese_monarchy
government_rank = 1
mercantilism = 0.0
primary_culture = kansai
religion = mahayana
technology_group = chinese
capital = 2287

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1339.1.1 = {
	monarch = {
		name = "Yoritoo"
		dynasty = "Toki"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1342.12.29 = {
	monarch = {
		name = "Yoriyasu"
		dynasty = "Toki"
		ADM = 4
		DIP = 4
		MIL = 4
	}
}

1388.2.3 = {
	monarch = {
		name = "Yasuyuki"
		dynasty = "Toki"
		ADM = 3
		DIP = 2
		MIL = 2
	}
}

1390.3.1 = {
	monarch = {
		name = "Yoritada"
		dynasty = "Toki"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Yorimasu"
		monarch_name = "Yorimasu"
		dynasty = "Toki"
		birth_date = 1351.1.1
		death_date = 1414.4.23
		claim = 80
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1397.9.2 = {
	monarch = {
		name = "Yorimasu"
		dynasty = "Toki"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1406.1.1 = {
	heir = {
		name = "Mochimasu"
		monarch_name = "Mochimasu"
		dynasty = "Toki"
		birth_date = 1406.1.1
		death_date = 1474.10.7
		claim = 80
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1414.4.23 = {
	monarch = {
		name = "Mochimasu"
		dynasty = "Toki"
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Mochimasu"
		monarch_name = "Mochimasu"
		dynasty = "Toki"
		birth_date = 1406.1.1
		death_date = 1474.10.7
		claim = 80
		ADM = 2
		DIP = 2
		MIL = 2
	}
}

1442.1.1 = {
	heir = {
		name = "Shigeyori"
		monarch_name = "Shigeyori"
		dynasty = "Toki"
		birth_date = 1442.1.1
		death_date = 1497.5.5
		claim = 70
		ADM = 2
		DIP = 3
		MIL = 2
	}
}

1467.1.1 = {
	monarch = {
		name = "Shigeyori"
		dynasty = "Toki"
		ADM = 2
		DIP = 3
		MIL = 2
	}
	heir = {
		name = "Nasafusa"
		monarch_name = "Masafusa"
		dynasty = "Toki"
		birth_date = 1457.1.1
		death_date = 1519.7.12
		claim = 70
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1495.1.1 = {
	monarch = {
		name = "Masafusa"
		dynasty = "Toki"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}

1498.1.1 = {
	heir = {
		name = "Yoritake"
		monarch_name = "Yoritake"
		dynasty = "Toki"
		birth_date = 1488.1.1
		death_date = 1536.1.1
		claim = 50
		ADM = 1
		DIP = 1
		MIL = 2
	}
}

1535.6.1 = {
	monarch = {
		name = "Yorinori"
		dynasty = "Toki"
		ADM = 1
		DIP = 1
		MIL = 1
	}
}
