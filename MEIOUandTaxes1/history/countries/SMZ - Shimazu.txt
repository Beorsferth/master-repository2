# SMZ - Shimazu clan
# LS/GG - Japanese Civil War

government = japanese_monarchy
government_rank = 1
# aristocracy_plutocracy = -4
# centralization_decentralization = 2
# innovative_narrowminded = 2
mercantilism = 0.0 # mercantilism_freetrade = -5
# offensive_defensive = -1
# land_naval = 0
# quality_quantity = 4
# serfdom_freesubjects = -5
# isolationist_expansionist = -3
# secularism_theocracy = 0
primary_culture = kyushu
religion = mahayana
technology_group = chinese
capital = 1015		# Satsuma

1000.1.1 = {
	add_country_modifier = { name = title_3 duration = -1 }
	set_country_flag = title_3
	#set_variable = { which = "centralization_decentralization" value = 2 }
	add_absolutism = -100
	add_absolutism = 30
}

1325.11.12 = {
	monarch = {
		name = "Sadahisa"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1328.1.1 = {
	heir = {
		name = "Ujihisa"
		monarch_name = "Ujihisa"
		dynasty = "Shimazu"
		birth_date = 1328.1.1
		death_date = 1387.6.30
		claim = 80
		ADM = 3
		DIP = 4
		MIL = 4
		leader = {
			name = "Ujihisa Shimazu"
			type = general
			fire = 3
			shock = 4
			manuever = 2
			siege = 2
		}
	}
}

1363.8.12 = {
	monarch = {
		name = "Ujihisa"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 4
		MIL = 4
		leader = {
			name = "Ujihisa Shimazu"
			type = general
			fire = 3
			shock = 4
			manuever = 2
			siege = 2
		}
	}
}

1363.8.12 = {
	heir = {
		name = "Motohisa"
		monarch_name = "Motohisa"
		dynasty = "Shimazu"
		birth_date = 1363.7.1
		death_date = 1411.8.25
		claim = 95
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1387.6.30 = {
	monarch = {
		name = "Motohisa"
		dynasty = "Shimazu"
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1387.6.30 = {
	heir = {
		name = "Hisatoyo"
		monarch_name = "Hisatoyo"
		dynasty = "Shimazu"
		birth_date = 1375.1.1
		death_date = 1425.1.1
		claim = 60
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1411.8.25 = {
	monarch = {
		name = "Hisatoyo"
		dynasty = "Shimazu"
		ADM = 2
		DIP = 2
		MIL = 3
	}
}

1411.8.25 = {
	heir = {
		name = "Tadakuni"
		monarch_name = "Tadakuni"
		dynasty = "Shimazu"
		birth_date = 1403.5.22
		death_date = 1470.2.21
		claim = 95
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1425.1.1 = {
	monarch = {
		name = "Tadakuni"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 3
		MIL = 4
	}
}

1432.11.27 = {
	heir = {
		name = "Tatsuhisa"
		monarch_name = "Tatsuhisa"
		dynasty = "Shimazu"
		birth_date = 1432.11.27
		death_date = 1474.4.26
		claim = 95
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1470.2.21 = {
	monarch = {
		name = "Tatsuhisa"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 4
		MIL = 3
	}
}

1470.2.21 = {
	heir = {
		name = "Tadamasa"
		monarch_name = "Tadamasa"
		dynasty = "Shimazu"
		birth_date = 1463.5.21
		death_date = 1508.3.16
		claim = 75
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1474.4.26 = {
	monarch = {
		name = "Tadamasa"
		dynasty = "Shimazu"
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1489.2.17 = {
	heir = {
		name = "Tadaharu"
		monarch_name = "Tadaharu"
		dynasty = "Shimazu"
		birth_date = 1489.2.17
		death_date = 1515.8.25
		claim = 75
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1508.3.16 = {
	monarch = {
		name = "Tadaharu"
		dynasty = "Shimazu"
		ADM = 2
		DIP = 2
		MIL = 1
	}
}

1515.8.25 = {
	monarch = {
		name = "Tadataka"
		dynasty = "Shimazu"
		ADM = 2
		DIP = 1
		MIL = 1
	}
}

1519.5.12 = {
	monarch = {
		name = "Katsuhisa"
		dynasty = "Shimazu"
		ADM = 1
		DIP = 2
		MIL = 1
	}
}

1520.1.1 = {
	heir = {
		name = "Takahisa"
		monarch_name = "Takahisa"
		dynasty = "Shimazu"
		birth_date = 1514.5.28
		death_date = 1571.7.15
		claim = 80
		ADM = 4
		DIP = 4
		MIL = 5
	}
}

1527.1.1 = {
	monarch = {
		name = "Takahisa"
		dynasty = "Shimazu"
		ADM = 4
		DIP = 4
		MIL = 5
	}
}

1533.3.4 = {
	heir = {
		name = "Yoshihisa"
		monarch_name = "Yoshihisa"
		dynasty = "Shimazu"
		birth_date = 1533.3.4
		death_date = 1611.3.5
		claim = 80
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1566.1.1 = {
	monarch = {
		name = "Yoshihisa"
		dynasty = "Shimazu"
		ADM = 5
		DIP = 4
		MIL = 5
	}
}

1587.5.22 = {
	monarch = {
		name = "Yoshihiro"
		dynasty = "Shimazu"
		ADM = 4
		DIP = 4
		MIL = 6
	}
}

1587.5.22 = {
	heir = {
		name = "Tadatsune"
		monarch_name = "Tadatsune"
		dynasty = "Shimazu"
		birth_date = 1576.11.27
		death_date = 1638.4.7
		claim = 90
		ADM = 4
		DIP = 4
		MIL = 3
	}
}

1619.8.20 = {
	monarch = {
		name = "Tadatsune"
		dynasty = "Shimazu"
		ADM = 4
		DIP = 4
		MIL = 3
	}
	heir = {
		name = "Mitsuhisa"
		monarch_name = "Mitsuhisa"
		dynasty = "Shimazu"
		birth_date = 1616.7.15
		death_date = 1695.1.14
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1638.4.7 = {
	monarch = {
		name = "Mitsuhisa"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 3
		MIL = 3
	}
	heir = {
		name = "Tsunahisa"
		monarch_name = "Tsunahisa"
		dynasty = "Shimazu"
		birth_date = 1632.5.19
		death_date = 1695.1.14
		claim = 90
		ADM = 3
		DIP = 3
		MIL = 3
	}
}

1668.4.7 = {
	monarch = {
		name = "Tsunahisa"
		dynasty = "Shimazu"
		ADM = 3
		DIP = 3
		MIL = 3
	}
}
