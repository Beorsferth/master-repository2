# ARB - Giudicato di Arborea

government = feudal_monarchy government_rank = 1
mercantilism = 0.0
technology_group = western
religion = catholic
primary_culture = sardinian
capital = 2241 # Arborea

1000.1.1 = {
	add_country_modifier = { name = title_5 duration = -1 }
	set_country_flag = title_5
}

1347.1.1 = {
	monarch = {
		name = "Marianus IV"
		dynasty = "de Bas-Serra"
		birth_date = 1329.1.5
		DIP = 4
		MIL = 3
		ADM = 4
	}
	queen = {
		country_of_origin = ARA
		name = "Timbor"
		dynasty = "de Rocaberti"
		birth_date = 1330.2.3
		death_date = 1378.2.6
		female = yes
		ADM = 2
		DIP = 2
		MIL = 2
	}
	heir = {
		name = "Ugone"
		monarch_name = "Ugone III Cappai"
		dynasty = "de Bas-Serra"
		birth_date = 1342.2.3
		death_date = 1383.2.6
		claim = 95
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

1356.1.1 = {
	#set_variable = { which = "centralization_decentralization" value = 4 }
	add_absolutism = -100
	add_absolutism = 20
}

1376.1.1 = {
	monarch = {
		name = "Ugone III Cappai"
		dynasty = "de Bas-Serra"
		birth_date = 1342.2.3
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

1383.1.1 = {
	monarch = {
		name = "Eleonora"
		dynasty = "de Bas-Serra"
		birth_date = 1347.6.2
		female = yes
		regent = yes
		DIP = 4
		MIL = 3
		ADM = 4
	}
	heir = {
		name = "Frederico"
		monarch_name = "Frederico"
		dynasty = "de Bas-Serra"
		birth_date = 1377.2.3
		death_date = 1387.2.6
		claim = 95
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

1387.2.6 = {
	monarch = {
		name = "Eleonora"
		dynasty = "de Bas-Serra"
		birth_date = 1347.6.2
		female = yes
		regent = yes
		DIP = 4
		MIL = 3
		ADM = 4
	}
	heir = {
		name = "Marianus"
		monarch_name = "Marianus V"
		dynasty = "de Bas-Serra"
		birth_date = 1378.2.3
		death_date = 1407.2.6
		claim = 95
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

1394.2.3 = {
	monarch = {
		name = "Marianus V"
		dynasty = "de Bas-Serra"
		birth_date = 1378.2.3
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

1407.1.1 = {
	monarch = {
		name = "Leonardo"
		dynasty = "Cubello"
		birth_date = 1378.2.3
		DIP = 4
		MIL = 3
		ADM = 4
	}
	government = feudal_monarchy remove_country_modifier = title_5 clr_country_flag = title_5 add_country_modifier = { name = title_2 duration = -1 }
	set_country_flag = title_2
}

1410.1.1 = {
	monarch = {
		name = "Guglielmo III"
		dynasty = "de Bas-Serra"
		birth_date = 1370.2.3
		DIP = 4
		MIL = 3
		ADM = 4
	}
}

# Sold to Alfonso the Magnanimous on 17 August 1420 for 100,000 gold florins.
