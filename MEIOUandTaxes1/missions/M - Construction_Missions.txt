# Construction Missions

construct_grand_fleet = {
	
	type = country
	
	category = ADM
	
	allow = {
		NOT = { has_country_flag = constructed_grand_fleet_mission }
		NOT = { has_country_modifier = naval_enthusiasm_mission }
		NOT = { has_country_modifier = thriving_ship_building_sector }
		num_of_ports = 5
		naval_forcelimit = 40
		technology_group = western
		NOT = { num_of_heavy_ship = 15 }
		num_of_heavy_ship = 5
		full_idea_group = naval_ideas
		NOT = { technology_group = nomad_group }
	}
	immediate = {
		add_country_modifier = {
			name = "thriving_ship_building_sector"
			duration = 3650
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	abort_effect = {
		remove_country_modifier = thriving_ship_building_sector
		add_country_modifier = {
			name = "thriving_ship_building_sector_malus"
			duration = 1825
		}
	}
	success = {
		num_of_heavy_ship = 20
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.05
			num_of_ports = 10
		}
		modifier = {
			factor = 1.05
			prestige = 10
		}
		modifier = {
			factor = 2.0
			has_idea = press_gangs
		}
		modifier = {
			factor = 1.5
			has_idea = grand_navy
		}
		modifier = {
			factor = 2.0
			has_idea = sea_hawks
		}
		modifier = {
			factor = 2.0
			has_idea = superior_seamanship
		}
		modifier = {
			factor = 2.0
			has_idea = naval_fighting_instruction
		}
		modifier = {
			factor = 2.0
			has_idea = excellent_shipwrights
		}
		modifier = {
			factor = 2.0
			has_idea = naval_glory
		}
	}
	effect = {
		add_navy_tradition = 10
		add_prestige = 5
		add_country_modifier = {
			name = "naval_enthusiasm_mission"
			duration = 3650
		}
		set_country_flag = constructed_grand_fleet_mission
		remove_country_modifier = thriving_ship_building_sector
	}
}


rule_the_seas = {
	
	type = country
	
	category = ADM
	
	allow = {
		num_of_ports = 5
		num_of_heavy_ship = 25
		naval_forcelimit = 25
		technology_group = western
		full_idea_group = naval_ideas
		NOT = {
			has_country_modifier = naval_enthusiasm_mission
		}
		NOT = { has_country_modifier = thriving_ship_building_sector }
		OR = {
			NOT = { has_country_flag = rule_the_seas_mission }
			had_country_flag = { flag = rule_the_seas_mission days = 7300 }
		}
		any_known_country = {
			num_of_heavy_ship = FROM
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	immediate = {
		add_country_modifier = {
			name = "thriving_ship_building_sector"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = thriving_ship_building_sector
		add_country_modifier = {
			name = "thriving_ship_building_sector_malus"
			duration = 1825
		}
	}
	success = {
		NOT = {
			any_known_country = {
				num_of_heavy_ship = FROM
			}
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.15
			num_of_ports = 10
		}
		modifier = {
			factor = 1.1
			prestige = 10
		}
		modifier = {
			factor = 1.25
			has_idea = press_gangs
		}
		modifier = {
			factor = 10.0
			has_idea = grand_navy
		}
		modifier = {
			factor = 2.0
			has_idea = sea_hawks
		}
		modifier = {
			factor = 2.0
			has_idea = superior_seamanship
		}
		modifier = {
			factor = 2.0
			has_idea = naval_fighting_instruction
		}
		modifier = {
			factor = 2.0
			has_idea = excellent_shipwrights
		}
		modifier = {
			factor = 2.0
			has_idea = naval_glory
		}
	}
	effect = {
		add_navy_tradition = 10
		set_country_flag = rule_the_seas_mission
		add_country_modifier = {
			name = "naval_enthusiasm_mission"
			duration = 3650
		}
		add_prestige = 5
		remove_country_modifier = thriving_ship_building_sector
	}
}


construct_shipyard = {
	
	type = country
	
	category = ADM
	ai_mission = yes
	
	allow = {
		is_at_war = no
		num_of_ports = 5
		dip_tech = 18
		technology_group = western
		has_idea_group = naval_ideas
		NOT = {
			has_country_modifier = expansion_of_the_shipbuilding_industry
		}
		NOT = { has_country_modifier = construction_building_mission }
		NOT = { military_harbour_1 = 1 }
		NOT = { military_harbour_2 = 1 }
		NOT = { military_harbour_3 = 1 }
		NOT = { military_harbour_4 = 1 }
		any_owned_province = {
			can_build = military_harbour_1
		}
	}
	abort = {
		NOT = { num_of_ports = 1 }
	}
	immediate = {
		add_country_modifier = {
			name = "construction_building_mission"
			duration = 1825
		}
	}
	abort_effect = {
		remove_country_modifier = construction_building_mission
		add_country_modifier = {
			name = "construction_building_mission_malus"
			duration = 1825
		}
	}
	success = {
		military_harbour_1 = 1
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			num_of_ports = 10
		}
		modifier = {
			factor = 1.5
			prestige = 10
		}
		modifier = {
			factor = 2.0
			has_idea = press_gangs
		}
		modifier = {
			factor = 10.0
			has_idea = grand_navy
		}
		modifier = {
			factor = 2.0
			has_idea = sea_hawks
		}
		modifier = {
			factor = 2.0
			has_idea = superior_seamanship
		}
		modifier = {
			factor = 2.0
			has_idea = naval_fighting_instruction
		}
		modifier = {
			factor = 2.0
			has_idea = excellent_shipwrights
		}
		modifier = {
			factor = 2.0
			has_idea = naval_glory
		}
	}
	effect = {
		add_navy_tradition = 25
		define_advisor = { type = naval_reformer skill = 1 discount = yes }
		add_country_modifier = {
			name = "expansion_of_the_shipbuilding_industry"
			duration = 3650
		}
	}
}



improve_key_defensive_position_small = {
	
	type = our_provinces
	
	category = ADM
	ai_mission = yes
	
	allow = {
		has_building = fort_14th
		NOT = { has_building = fort_15th }
		can_build = fort_15th
		NOT = { can_build = fort_16th }
		owner = {
			is_at_war = no
			NOT = { has_country_modifier = construction_building_mission }
		}
		any_neighbor_province = {
			NOT = { owned_by = FROM }
		}
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	immediate = {
		owner = {
			add_country_modifier = {
				name = "construction_building_mission"
				duration = 1000
			}
		}
	}
	abort_effect = {
		owner = {
			remove_country_modifier = construction_building_mission
			add_country_modifier = {
				name = "construction_building_mission_malus"
				duration = 1825
			}
		}
	}
	success = {
		has_building = fort_15th
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					has_casus_belli_against = FROM
				}
			}
		}
		modifier = {
			factor = 1.5
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			base_tax = 5
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					NOT = { religion_group = FROM }
				}
			}
		}
	}
	effect = {
		owner = {
			add_army_tradition = 5
			define_advisor = { type = fortification_expert skill = 1 discount = yes }
		}
	}
}


improve_key_defensive_position_medium = {
	
	type = our_provinces
	
	category = ADM
	ai_mission = yes
	
	allow = {
		OR = {
			has_building = fort_14th
			has_building = fort_15th
		}
		NOT = { has_building = fort_16th }
		can_build = fort_16th
		NOT = { can_build = fort_17th }
		owner = {
			is_at_war = no
			NOT = { has_country_modifier = construction_building_mission }
		}
		any_neighbor_province = {
			NOT = { owned_by = FROM }
		}
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	immediate = {
		owner = {
			add_country_modifier = {
				name = "construction_building_mission"
				duration = 1000
			}
		}
	}
	abort_effect = {
		owner = {
			remove_country_modifier = construction_building_mission
			add_country_modifier = {
				name = "construction_building_mission_malus"
				duration = 1825
			}
		}
	}
	success = {
		has_building = fort_16th
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					has_casus_belli_against = FROM
				}
			}
		}
		modifier = {
			factor = 1.5
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			base_tax = 5
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					NOT = { religion_group = FROM }
				}
			}
		}
	}
	effect = {
		owner = {
			add_army_tradition = 5
			define_advisor = { type = fortification_expert skill = 2 discount = yes }
		}
	}
}


improve_key_defensive_position_hard = {
	
	type = our_provinces
	
	category = ADM
	ai_mission = yes
	
	allow = {
		OR = {
			has_building = fort_14th
			has_building = fort_15th
			has_building = fort_16th
		}
		NOT = { has_building = schools }
		NOT = { has_building = arsenal }
		NOT = { has_building = naval_arsenal }
		NOT = { has_building = mint }
		NOT = { has_building = post_system }
		NOT = { has_building = fort_17th }
		can_build = fort_17th
		NOT = { can_build = fort_18th }
		owner = {
			is_at_war = no
			NOT = { has_country_modifier = construction_building_mission }
		}
		any_neighbor_province = {
			NOT = { owned_by = FROM }
		}
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	immediate = {
		owner = {
			add_country_modifier = {
				name = "construction_building_mission"
				duration = 1000
			}
		}
	}
	abort_effect = {
		owner = {
			remove_country_modifier = construction_building_mission
			add_country_modifier = {
				name = "construction_building_mission_malus"
				duration = 1825
			}
		}
	}
	success = {
		has_building = fort_17th
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					has_casus_belli_against = FROM
				}
			}
		}
		modifier = {
			factor = 1.5
			is_capital = yes
		}
		modifier = {
			factor = 1.5
			base_tax = 5
		}
		modifier = {
			factor = 2.0
			any_neighbor_province = {
				owner = {
					NOT = { religion_group = FROM }
				}
			}
		}
	}
	effect = {
		owner = {
			add_army_tradition = 5
			define_advisor = { type = fortification_expert skill = 3 discount = yes }
		}
	}
}







build_university = {
	
	type = country
	
	category = ADM
	ai_mission = yes
	
	allow = {
		technology_group = western
		is_at_war = no
		NOT = { has_country_modifier = construction_building_mission }
		NOT = { medium_university = 1 }
		NOT = { big_university = 1 }
		NOT = { small_university = 1 }
		any_owned_province = {
			can_build = small_university
		}
	}
	abort = {
	}
	immediate = {
		add_country_modifier = {
			name = "construction_building_mission"
			duration = 1000
		}
	}
	abort_effect = {
		remove_country_modifier = construction_building_mission
		add_country_modifier = {
			name = "construction_building_mission_malus"
			duration = 1825
		}
	}
	success = {
		OR = {
			small_university = 1
			medium_university = 1
			big_university = 1
		}
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.5
			monthly_income = 30
		}
		modifier = {
			factor = 1.5
			has_idea_group = culture_ideas
		}
		modifier = {
			factor = 2
			OR = {
				has_idea_group = theology_ideas
				has_idea_group = innovativeness_ideas
			}
		}
	}
	effect = {
		define_advisor = { type = philosopher skill = 2 discount = yes }
		add_prestige = 10
		add_adm_power = 50
	}
}


construct_army = {
	
	type = country
	
	category = ADM
	
	allow = {
		is_year = 1358
		
		OR = {
			NOT = { has_country_flag = construct_army_mission }
			had_country_flag = { flag = construct_army_mission days = 7300 }
		}
		NOT = { army_size_percentage = 0.5 }
		NOT = { has_country_modifier = thriving_arms_industry }
		NOT = { has_country_modifier = land_enthusiasm_mission }
		is_emperor = no
	}
	abort = {
	}
	immediate = {
		add_country_modifier = {
			name = "thriving_arms_industry"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = thriving_arms_industry
		add_country_modifier = {
			name = "thriving_arms_industry_malus"
			duration = 1825
		}
	}
	success = {
		army_size_percentage = 0.75
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2.0
			is_at_war = yes
		}
		modifier = {
			factor = 2.0
			NOT = { army_size_percentage = 0.5 }
		}
		modifier = {
			factor = 2.0
			NOT = { army_size_percentage = 0.3 }
		}
		modifier = {
			factor = 1.5
			has_idea = grand_army
		}
		modifier = {
			factor = 1.5
			has_idea = military_drill
		}
		modifier = {
			factor = 1.5
			has_idea = engineer_corps
		}
		modifier = {
			factor = 1.5
			has_idea = battlefield_commisions
		}
		modifier = {
			factor = 1.5
			has_idea = glorious_arms
		}
		modifier = {
			factor = 2.0
			has_idea = national_conscripts
		}
		modifier = {
			factor = 2.0
			has_idea = regimental_system
		}
		modifier = {
			factor = 2.0
			has_idea = military_treatises
		}
	}
	effect = {
		set_country_flag = construct_army_mission
		add_army_tradition = 10
		add_prestige = 5
		remove_country_modifier = thriving_arms_industry
		add_country_modifier = {
			name = "land_enthusiasm_mission"
			duration = 3650
		}
	}
}


construct_navy = {
	
	type = country
	
	category = ADM
	
	allow = {
		primitives = no
		any_owned_province = {
			has_port = yes
			is_core = FROM
		}
		full_idea_group = naval_ideas
		OR = {
			NOT = { has_country_flag = construct_navy_mission }
			had_country_flag = { flag = construct_navy_mission days = 7300 }
		}
		NOT = { has_country_modifier = thriving_ship_building_sector }
		NOT = { has_country_modifier = naval_enthusiasm_mission }
		NOT = { navy_size_percentage = 0.5 }
		NOT = { technology_group = sub_saharan }
		NOT = { technology_group = north_american }
		NOT = { technology_group = mesoamerican }
		NOT = { technology_group = south_american }
		NOT = { technology_group = nomad_group }
	}
	abort = {
		NOT = {
			any_owned_province = {
				has_port = yes
				is_core = FROM
			}
		}
	}
	immediate = {
		add_country_modifier = {
			name = "thriving_ship_building_sector"
			duration = 3650
		}
	}
	abort_effect = {
		remove_country_modifier = thriving_ship_building_sector
		add_country_modifier = {
			name = "thriving_ship_building_sector_malus"
			duration = 1825
		}
	}
	success = {
		navy_size_percentage = 0.75
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 2.0
			is_at_war = yes
		}
		modifier = {
			factor = 2.0
			NOT = { navy_size_percentage = 0.5 }
		}
		modifier = {
			factor = 2.0
			NOT = { navy_size_percentage = 0.3 }
		}
		modifier = {
			factor = 2.0
			has_idea = press_gangs
		}
		modifier = {
			factor = 2.0
			has_idea = grand_navy
		}
		modifier = {
			factor = 2.0
			has_idea = sea_hawks
		}
		modifier = {
			factor = 2.0
			has_idea = superior_seamanship
		}
		modifier = {
			factor = 2.0
			has_idea = naval_fighting_instruction
		}
		modifier = {
			factor = 2.0
			has_idea = excellent_shipwrights
		}
		modifier = {
			factor = 2.0
			has_idea = naval_glory
		}
	}
	effect = {
		set_country_flag = construct_navy_mission
		add_navy_tradition = 10
		add_prestige = 5
		remove_country_modifier = thriving_ship_building_sector
		add_country_modifier = {
			name = "naval_enthusiasm_mission"
			duration = 3650
		}
	}
}


fortify_province = {
	
	type = our_provinces
	
	category = ADM
	ai_mission = yes
	
	allow = {
		NOT = { has_building = fort_14th }
		can_build = fort_14th
		owner = { is_at_war = no }
		is_colony = no
		owner = {
			NOT = { fort_14th = 1 }
			NOT = { has_country_modifier = construction_building_mission }
			NOT = { has_country_flag = had_fort_mission }
		}
	}
	abort = {
		NOT = { owned_by = FROM }
	}
	immediate = {
		owner = {
			add_country_modifier = {
				name = "construction_building_mission"
				duration = 1000
			}
		}
	}
	abort_effect = {
		owner = {
			remove_country_modifier = construction_building_mission
			add_country_modifier = {
				name = "construction_building_mission_malus"
				duration = 1825
			}
		}
	}
	success = {
		has_building = fort_14th
	}
	chance = {
		factor = 1000
		modifier = {
			factor = 1.2
			any_neighbor_province = {
				owner = {
					has_casus_belli_against = FROM
				}
			}
		}
		modifier = {
			factor = 1.25
			is_capital = yes
		}
		modifier = {
			factor = 1.12
			base_tax = 6
		}
		modifier = {
			factor = 1.07
			any_neighbor_province = {
				owner = {
					NOT = { religion_group = FROM }
				}
			}
		}
	}
	effect = {
		#add_base_manpower = 1
		owner = {
			add_army_tradition = 5
			set_country_flag = had_fort_mission
		}
	}
}


rival_army = {
	
	type = rival_countries
	
	category = ADM
	
	allow = {
		FROM = {
			NOT = { army_size = ROOT }
			NOT = { has_country_modifier = thriving_arms_industry }
			NOT = { has_country_modifier = land_enthusiasm_mission }
			OR = {
				NOT = { has_country_flag = rival_army_mission }
				had_country_flag = { flag = rival_army_mission days = 7300 }
			}
		}
		NOT = { alliance_with = FROM }
		NOT = { is_subject_of = FROM }
		NOT = { overlord_of = FROM }
	}
	abort = {
		OR = {
			alliance_with = FROM
			is_subject_of = FROM
			overlord_of = FROM
		}
	}
	immediate = {
		FROM = {
			add_country_modifier = {
				name = "thriving_arms_industry"
				duration = 3650
			}
		}
	}
	abort_effect = {
		FROM = {
			remove_country_modifier = thriving_arms_industry
			add_country_modifier = {
				name = "thriving_arms_industry_malus"
				duration = 1825
			}
		}
	}
	success = {
		FROM = { army_size = ROOT }
	}
	chance = {
		factor = 1000
		
		modifier = {
			factor = 1.5
			FROM = { mil = 3 }
		}
	}
	effect = {
		FROM = {
			set_country_flag = rival_army_mission
			add_army_tradition = 5
			remove_country_modifier = thriving_arms_industry
			add_country_modifier = {
				name = "land_enthusiasm_mission"
				duration = 3650
			}
		}
	}
}


rival_navy = {
	
	type = rival_countries
	
	category = ADM
	
	allow = {
		FROM = {
			NOT = { navy_size = ROOT }
			NOT = { has_country_modifier = naval_enthusiasm_mission }
			NOT = { has_country_modifier = thriving_ship_building_sector }
			naval_forcelimit = ROOT
			OR = {
				NOT = { has_country_flag = had_rival_navy_mission }
				had_country_flag = { flag = had_rival_navy_mission days = 7300 }
			}
		}
		NOT = { alliance_with = FROM }
		NOT = { is_subject_of = FROM }
		NOT = { overlord_of = FROM }
		num_of_ports = 3
		FROM = {
			NOT = { technology_group = sub_saharan }
			NOT = { technology_group = north_american }
			NOT = { technology_group = mesoamerican }
			NOT = { technology_group = south_american }
			NOT = { technology_group = nomad_group }
			NOT = { technology_group = steppestech }
			NOT = { technology_group = austranesian }
			any_owned_province = {
				has_port = yes
				is_core = owner
			}
		}
	}
	abort = {
		OR = {
			FROM = {
				NOT = {
					any_owned_province = {
						has_port = yes
						is_core = owner
					}
				}
			}
			alliance_with = FROM
			is_subject_of = FROM
			overlord_of = FROM
		}
	}
	immediate = {
		FROM = {
			add_country_modifier = {
				name = "thriving_ship_building_sector"
				duration = 3650
			}
		}
	}
	abort_effect = {
		FROM = {
			remove_country_modifier = thriving_ship_building_sector
			add_country_modifier = {
				name = "thriving_ship_building_sector_malus"
				duration = 1825
			}
		}
	}
	success = {
		FROM = { navy_size = ROOT }
	}
	chance = {
		factor = 1000
		
		modifier = {
			factor = 1.5
			FROM = { mil = 3 }
		}
	}
	effect = {
		FROM = {
			set_country_flag = had_rival_navy_mission
			add_navy_tradition = 10
			add_prestige = 5
			remove_country_modifier = thriving_ship_building_sector
			add_country_modifier = {
				name = "naval_enthusiasm_mission"
				duration = 3650
			}
		}
	}
}
