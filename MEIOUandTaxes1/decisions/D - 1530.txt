country_decisions = {
	
	little_war_in_hungary = {
		potential = {
			NOT = { has_country_flag = little_war_in_hungary }
			tag = HAB
			has_global_flag = 1530_start_date
		}
		allow = {
			is_at_war = yes
			OR = {
				AND = {
					ai = yes
					is_year = 1532
				}
				ai = no
			}
		}
		effect = {
			set_country_flag = little_war_in_hungary
			add_war_exhaustion = -4
			add_army_tradition = 15
			add_mil_power = 100
			if = {
				limit = {
					capital_scope = { controlled_by = ROOT }
				}
				capital_scope = {
					build_to_forcelimit = {
						infantry = 0.3
						cavalry = 0.1
						artillery = 0.1
					}
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	#Silesia
	
	piast_dynasty_minor = {
		potential = {
			NOT = { has_country_flag = piast_dynasty }
			dynasty = "Piast"
			primary_culture = silesian
		}
		allow = {
		}
		effect = {
			set_country_flag = piast_dynasty
			add_country_modifier = {
				name = piast_dynasty
				duration = -1
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	#India
	
	#Help for Suri
	
	sher_shah_suri = {
		potential = {
			NOT = { has_country_flag = sher_shah_suri }
			tag = TRT
			has_global_flag = 1530_start_date
		}
		allow = {
			is_at_war = no
		}
		effect = {
			set_country_flag = sher_shah_suri
			add_war_exhaustion = -10
			add_army_tradition = 30
			add_mil_power = 100
			add_manpower = 20
			random_owned_province = {
				limit = {
					is_capital = yes
					controlled_by = ROOT
				}
				build_to_forcelimit = {
					infantry = 0.6
					cavalry = 0.2
					artillery = 0.2
				}
			}
			BNG = {
				every_owned_province = {
					add_core = TRT
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	annex_bengal = {
		potential = {
			NOT = { has_country_flag = annex_bengal }
			tag = TRT
			has_global_flag = 1530_start_date
			BNG = { ai = yes }
			war_with = BNG
		}
		allow = {
			2693 = { controlled_by = ROOT }
			war_score_against = {
				who = BNG
				value = 1
			}
		}
		effect = {
			set_country_flag = annex_bengal
			BNG = {
				every_ally = {
					limit = {
						war_with = TRT
					}
					white_peace = TRT
				}
			}
			inherit = BNG
		}
		ai_will_do = {
			factor = 1
		}
	}
	
	
	#Hindustan
	
	#Gujarat
	
	annex_malwa = {
		potential = {
			NOT = { has_country_flag = annex_malwa }
			tag = GUJ
			has_global_flag = 1530_start_date
			MLW = { ai = yes }
			war_with = MLW
		}
		allow = {
			2211 = { controlled_by = ROOT }
			war_score_against = {
				who = MLW
				value = 1
			}
		}
		effect = {
			set_country_flag = annex_malwa
			MLW = {
				every_ally = {
					limit = {
						war_with = GUJ
					}
					white_peace = GUJ
				}
				520 = {
					remove_core = MLW
					cede_province = HDT
				}
				every_owned_province = {
					cede_province = GUJ
				}
			}
			change_title_5 = yes
			inherit = MLW
		}
		ai_will_do = {
			factor = 400
		}
	}
	#England
	give_up_calais = {
		potential = {
			NOT = { has_country_flag = give_up_calais }
			tag = ENG
			has_global_flag = 1530_start_date
			owns = 63 # Calais
			war_with = FRA
		}
		allow = {
			63 = { controlled_by = FRA }
		}
		effect = {
			set_country_flag = give_up_calais
			63 = { cede_province = FRA }
			white_peace = FRA
			every_ally = {
				limit = {
					war_with = FRA
				}
				white_peace = FRA
			}
			63 = {
				remove_core = ENG
				cede_province = FRA
				change_culture = picard
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	#Pizarro
	conquest_of_the_inca = {
		potential = {
			NOT = { has_country_flag = conquest_of_the_inca }
			tag = PIZ
			has_global_flag = 1530_start_date
		}
		allow = {
			OR = {
				is_year = 1531
				ai = no
			}
		}
		effect = {
			set_country_flag = conquest_of_the_inca
			hidden_effect = {
				QUI = {
					every_owned_province = {
						add_core = PIZ
						discover_country = ROOT
					}
				}
				CZC = {
					every_owned_province = {
						add_core = PIZ
						discover_country = ROOT
					}
				}
				set_ai_personality = {
					personality = ai_militarist
					locked = yes
				}
			}
			add_stability_6 = yes
			add_manpower = 20
			add_treasury = 1000
			add_country_modifier = {
				name = pizarro_conquest
				duration = -1
			}
			declare_war_with_cb = {
				who = CZC
				casus_belli = cb_conquest_of_paradise
			}
			declare_war_with_cb = {
				who = QUI
				casus_belli = cb_conquest_of_paradise
			}
			if = {
				limit = {
					capital_scope = { controlled_by = ROOT }
				}
				capital_scope = {
					build_to_forcelimit = {
						infantry = 0.8
						cavalry = 0.1
						artillery = 0.1
					}
				}
			}
		}
		ai_will_do = {
			factor = 400
		}
	}
	swear_fealty_to_spain = {
		potential = {
			NOT = { has_country_flag = swear_fealty_to_spain }
			tag = PIZ
			has_global_flag = 1530_start_date
		}
		allow = {
			owns = 793 # Quito
			owns = 2068 # Cuzco
			owns = 2345 # Tupiza
			is_at_war = no
		}
		effect = {
			set_country_flag = swear_fealty_to_spain
			SPA = {
				inherit = PIZ
				add_treasury = 1000
				add_inflation = 2
			}
			hidden_effect = {
				SPA = {
					every_owned_province = {
						remove_core = PIZ
					}
				}
				every_country = {
					every_owned_province = {
						remove_core = PIZ
					}
				}
			}
		}
		ai_will_do = {
			factor = 400
		}
	}
	
	#dev_increase
	dev_increase = {
		potential = {
			has_global_flag = 1530_start_date
			NOT = { has_global_flag = dev_increase_1530 }
		}
		allow = {
		}
		effect = {
			set_global_flag = dev_increase_1530
			every_country = {
				every_owned_province = {
					#7
					if = {
						limit = { base_tax = 7 }
						# add_base_tax = 1
					}
					if = {
						limit = { base_production = 7 }
						# add_base_production = 1
					}
					#6
					#5
					#4
					if = {
						limit = { base_production = 4 }
						# add_base_production = 1
					}
					if = {
						limit = { base_manpower = 4 }
						# add_base_manpower = 1
					}
					#3
					if = {
						limit = { base_tax = 3 }
						# add_base_tax = 1
					}
					if = {
						limit = { base_manpower = 4 }
						# add_base_manpower = 1
					}
					#2
					if = {
						limit = { base_production = 2 }
						# add_base_production = 1
					}
					if = {
						limit = { base_manpower = 2 }
						# add_base_manpower = 1
					}
					#1
					if = {
						limit = { base_tax = 1 }
						# add_base_tax = 1
					}
					if = {
						limit = { base_manpower = 1 }
						# add_base_manpower = 1
					}
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
	}
	
}
