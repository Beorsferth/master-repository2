country_decisions = {
	unite_the_clans_rightful = {
		potential = {
			government = irish_monarchy
		}
		allow = {
			OR = {
				adm_tech = 15
				
				ADM = 5
			}
			
			check_variable = { which = "Demesne_in_Ireland" value = 8 }
			adm_power = 400
			is_free_or_tributary_trigger = yes
			is_at_war = no
			ireland_region = {
				type = all
				OR = {
					owner = { primary_culture = irish }
					owner = { primary_culture = hiberno_norman }
				}
			}
		}
		effect = {
			add_adm_power = -400
			every_country = {
				limit = {
					OR = {
						primary_culture = hiberno_norman
						primary_culture = irish
					}
					capital_scope = {
						region = ireland_region
					}
					is_free_or_tributary_trigger = yes
				}
				ROOT = {
					vassalize = PREV
				}
			}
			every_country = {
				limit = {
					government = irish_monarchy
				}
				change_government = feudal_monarchy
			}
			ireland_region = {
				every_province = {
					remove_province_modifier = clan_land
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	unite_the_clans_panic = {
		potential = {
			government = irish_monarchy
		}
		allow = {
			OR = {
				adm_tech = 12
				ADM = 4
			}
			
			check_variable = { which = "Demesne_in_Ireland" value = 4 }
			adm_power = 400
			is_free_or_tributary_trigger = yes
			calc_true_if = {
				ireland_region = {
					type = all
					NOT = {
						OR = {
							owner = { primary_culture = irish }
							owner = { primary_culture = hiberno_norman }
						}
					}
				}
				amount = 8
			}
		}
		effect = {
			add_adm_power = -400
			every_country = {
				limit = {
					OR = {
						primary_culture = hiberno_norman
						primary_culture = irish
					}
					capital_scope = {
						region = ireland_region
					}
					is_free_or_tributary_trigger = yes
				}
				ROOT = {
					vassalize = PREV
				}
			}
			every_country = {
				limit = {
					government = irish_monarchy
				}
				change_government = feudal_monarchy
			}
			ireland_region = {
				every_province = {
					remove_province_modifier = clan_land
				}
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 400
	}
	
	break_the_irish_clans = {
		potential = {
			any_owned_province = {
				has_province_modifier = clan_land
				NOT = { nationalism = 1 }
			}
			NOT = {
				OR = {
					primary_culture = hiberno_norman
					primary_culture = irish
				}
			}
		}
		allow = {
			adm_tech = 20
			total_development = 300
			mil_power = 200
		}
		effect = {
			random_owned_province = {
				limit = {
					has_province_modifier = clan_land
					NOT = { nationalism = 1 }
				}
				
				add_core = IRE
				
				add_nationalism = 50
				
				if = {
					limit = {
						NOT = { owner = { total_development = 400 } }
					}
					random = {
						chance = 20
						add_scaled_local_mil_power = -10
						remove_province_modifier = clan_land
					}
				}
				if = {
					limit = {
						owner = { total_development = 400 }
						NOT = { owner = { total_development = 500 } }
					}
					random = {
						chance = 50
						add_scaled_local_mil_power = -10
						remove_province_modifier = clan_land
					}
				}
				if = {
					limit = {
						owner = { total_development = 500 }
						NOT = { owner = { total_development = 700 } }
					}
					random = {
						chance = 70
						add_scaled_local_mil_power = -10
						remove_province_modifier = clan_land
					}
				}
				if = {
					limit = {
						owner = { total_development = 700 }
					}
					random = {
						chance = 90
						add_scaled_local_mil_power = -10
						remove_province_modifier = clan_land
					}
				}
			}
		}
		ai_will_do = {
			factor = 1
			modifier = {
				factor = 0
				NOT = { total_development = 400 }
			}
			modifier = {
				factor = 0.5
				NOT = { total_development = 500 }
			}
			modifier = {
				factor = 2.0
				NOT = { total_development = 700 }
			}
		}
		ai_importance = 200
	}
}
