country_decisions = {
	create_kingdom_of_naples = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						area = naples_area
						area = calabria_area
						area = abruzzi_area
						area = apulia_area
					}
					NOT = { has_province_modifier = kingdom_of_naples }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_naples
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_kingdom_of_naples = {
		potential = {
			any_owned_province = {
				has_province_modifier = kingdom_of_naples
			}
		}
		allow = {
			always = yes
		}
		effect = {
			every_owned_province = {
				limit = {
					has_province_modifier = kingdom_of_naples
				}
				remove_province_modifier = kingdom_of_naples
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	#Sicily
	create_kingdom_of_sicily = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					region = sicily_region
					NOT = { has_province_modifier = kingdom_of_sicily }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_sicily
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			if = {
				limit = { NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_kingdom_of_sicily = {
		potential = {
			any_owned_province = {
				has_province_modifier = kingdom_of_sicily
			}
		}
		allow = {
			always = yes
		}
		effect = {
			every_owned_province = {
				limit = {
					has_province_modifier = kingdom_of_sicily
				}
				remove_province_modifier = kingdom_of_sicily
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	#Sardinia
	create_kingdom_of_sardinia = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 127 # Sassari
						province_id = 2241 # Aristanis
						province_id = 2242 # Casteddu
						province_id = 2852 # Gallura
					}
					NOT = { has_province_modifier = kingdom_of_sardinia }
				}
				add_permanent_province_modifier = {
					name = kingdom_of_sardinia
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
			if = {
				limit = { NOT = { has_country_modifier = title_5 has_country_modifier = title_6 } }
				change_title_5 = yes
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_kingdom_of_sardinia = {
		potential = {
			any_owned_province = {
				has_province_modifier = kingdom_of_sardinia
			}
		}
		allow = {
			always = yes
		}
		effect = {
			every_owned_province = {
				limit = {
					has_province_modifier = kingdom_of_sardinia
				}
				remove_province_modifier = kingdom_of_sardinia
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Low Countries
	#Artois
	create_county_of_artois = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 88 # Artoys
						province_id = 2307 # Guines
						province_id = 2353 # Cambresy
					}
					NOT = { has_province_modifier = county_of_artois }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_artois
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_artois = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_artois
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_artois
				}
				remove_province_modifier = county_of_artois
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	#Flanders
	create_county_of_flanders = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 87 # Flandres
						province_id = 90 # Brugge
						province_id = 2363 # Gent
					}
					NOT = { has_province_modifier = county_of_flanders }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_flanders
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_flanders = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_flanders
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_flanders
				}
				remove_province_modifier = county_of_flanders
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	#Mechelen #micro
	create_lordship_of_mechelen = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 2367 # Leuwen
					NOT = { has_province_modifier = lordship_of_mechelen }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_mechelen
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_lordship_of_mechelen = {
		potential = {
			any_owned_province = {
				has_province_modifier = lordship_of_mechelen
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = lordship_of_mechelen
				}
				remove_province_modifier = lordship_of_mechelen
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Namur
	create_county_of_namur = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 2364 # Nameur
					NOT = { has_province_modifier = county_of_namur }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_namur
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_namur = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_namur
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_namur
				}
				remove_province_modifier = county_of_namur
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	
	#Hainaut
	create_county_of_hainaut = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 91 # Hennault
					NOT = { has_province_modifier = county_of_hainaut }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_hainaut
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_hainaut = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_hainaut
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_hainaut
				}
				remove_province_modifier = county_of_hainaut
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Zeeland
	create_county_of_zeeland = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 96 # Zeeland
					NOT = { has_province_modifier = county_of_zeeland }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_zeeland
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_zeeland = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_zeeland
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_zeeland
				}
				remove_province_modifier = county_of_zeeland
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Holland
	create_county_of_holland = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 97 # Amsterdam
						province_id = 2370 # 's-Gravenhage
					}
					NOT = { has_province_modifier = county_of_holland }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_holland
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_holland = {
		potential = {
			any_owned_province = {
				has_province_modifier = county_of_holland
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_holland
				}
				remove_province_modifier = county_of_holland
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Brabant
	create_duchy_of_brabant = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 92 # Brussel
						province_id = 2367 # Leuwen
						province_id = 95 # Breda
						province_id = 2360 # Antwerpen
					}
					NOT = { has_province_modifier = duchy_of_brabant }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_brabant
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_duchy_of_brabant = {
		potential = {
			any_owned_province = {
				has_province_modifier = duchy_of_brabant
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = duchy_of_brabant
				}
				remove_province_modifier = duchy_of_brabant
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Limburg
	create_duchy_of_limburg = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 2795 # Limburg
					NOT = { has_province_modifier = duchy_of_limburg }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_limburg
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_duchy_of_limburg = {
		potential = {
			any_owned_province = {
				has_province_modifier = duchy_of_limburg
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = duchy_of_limburg
				}
				remove_province_modifier = duchy_of_limburg
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Luxembourg
	create_duchy_of_luxembourg = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 94 # Letzebuerg
						province_id = 1511 # Ardenne
					}
					NOT = { has_province_modifier = duchy_of_luxembourg }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_luxembourg
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_duchy_of_luxembourg = {
		potential = {
			any_owned_province = {
				has_province_modifier = duchy_of_luxembourg
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = duchy_of_luxembourg
				}
				remove_province_modifier = duchy_of_luxembourg
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Utrecht
	create_lordship_of_utrecht = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 98 # Utrecht
					NOT = { has_province_modifier = lordship_of_utrecht }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_utrecht
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_lordship_of_utrecht = {
		potential = {
			any_owned_province = {
				has_province_modifier = lordship_of_utrecht
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = lordship_of_utrecht
				}
				remove_province_modifier = lordship_of_utrecht
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Frisia
	create_lordship_of_frisia = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 1371 # Fryslan
					NOT = { has_province_modifier = lordship_of_frisia }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_frisia
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_lordship_of_frisia = {
		potential = {
			any_owned_province = {
				has_province_modifier = lordship_of_frisia
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = lordship_of_frisia
				}
				remove_province_modifier = lordship_of_frisia
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Guelders
	create_duchy_of_guelders = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 99 # Gelre
						province_id = 2449 # Gelder
					}
					NOT = { has_province_modifier = duchy_of_guelders }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = duchy_of_guelders
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_duchy_of_guelders = {
		potential = {
			any_owned_province = {
				has_province_modifier = duchy_of_guelders
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = duchy_of_guelders
				}
				remove_province_modifier = duchy_of_guelders
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Groningen #micro
	create_lordship_of_groningen = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 1371 # Fryslan
					NOT = { has_province_modifier = lordship_of_groningen }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_groningen
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_lordship_of_groningen = {
		potential = {
			any_owned_province = {
				has_province_modifier = lordship_of_groningen
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = lordship_of_groningen
				}
				remove_province_modifier = lordship_of_groningen
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Overijssel
	create_lordship_of_overijssel = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 1372 # het Sticht
						province_id = 2450 # Groningen
					}
					NOT = { has_province_modifier = lordship_of_overijssel }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = lordship_of_overijssel
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_lordship_of_overijssel = {
		potential = {
			any_owned_province = {
				has_province_modifier = lordship_of_overijssel
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = lordship_of_overijssel
				}
				remove_province_modifier = lordship_of_overijssel
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Zutphen #micro
	create_county_of_zutphen = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 99 # Gelre
					NOT = { has_province_modifier = county_of_zutphen }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = county_of_zutphen
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_county_of_zutphen = {
		potential = {
			always = no
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = county_of_zutphen
				}
				remove_province_modifier = county_of_zutphen
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#Tournai #micro
	create_bishopric_of_tournai = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					province_id = 87 # Flandres
					NOT = { has_province_modifier = bishopric_of_tournai }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = bishopric_of_tournai
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_bishopric_of_tournai = {
		potential = {
			any_owned_province = {
				has_province_modifier = bishopric_of_tournai
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = bishopric_of_tournai
				}
				remove_province_modifier = bishopric_of_tournai
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	#liege
	create_bishopric_of_liege = {
		potential = {
			always = no
		}
		allow = {
			always = no
		}
		effect = {
			every_owned_province = {
				limit = {
					OR = {
						province_id = 93 # Lidje
						province_id = 2451 # Loon
					}
					NOT = { has_province_modifier = bishopric_of_liege }
				}
				add_core = ROOT
				add_permanent_province_modifier = {
					name = bishopric_of_liege
					duration = -1
				}
				remove_province_modifier = at_cultural_uprising
				owner = { add_legitimacy = 1 }
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
	
	abrogate_bishopric_of_liege = {
		potential = {
			any_owned_province = {
				has_province_modifier = bishopric_of_liege
			}
		}
		allow = {
			always = yes
		}
		effect = {
			add_dip_power = -25
			every_owned_province = {
				limit = {
					has_province_modifier = bishopric_of_liege
				}
				remove_province_modifier = bishopric_of_liege
			}
		}
		ai_will_do = {
			factor = 1
		}
		ai_importance = 100
	}
}

